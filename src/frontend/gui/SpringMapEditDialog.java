/*
 * SpringMapEdit -- A 3D map editor for the Spring engine
 *
 * Copyright (C) 2008-2009  Heiko Schmitt <heikos23@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
/**
 * SpringMapEditDialog.java 
 * Created on 07.07.2008
 * by Heiko Schmitt
 */
package frontend.gui;

import java.awt.Toolkit;
import java.io.File;
import java.util.EnumMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Slider;

import backend.FileHandler;
import backend.SpringMapEdit;
import backend.FileHandler.FileFormat;
import backend.MapEditSettings.BrushMode;
import backend.image.Bitmap;
import frontend.gui.commands.Edit_Erode_Heightmap_Dry;
import frontend.gui.commands.Edit_Erode_Heightmap_Wet;
import frontend.render.MapRenderer;
import frontend.render.AppSettings;
import frontend.render.MapRenderer.MapMode;
import frontend.render.brushes.Brush;
import frontend.render.brushes.FeatureBrush.FeatureMode;
import frontend.render.brushes.HeightBrush.HeightMode;
import frontend.render.brushes.PrefabBrush.PrefabMode;
import frontend.render.brushes.TextureBrush.TextureMode;

/**
 * @author Heiko Schmitt
 *
 */
public class SpringMapEditDialog 
{	
	public Display display;
	public Shell shell;
	private boolean isUsingOwnDisplay;
	
	private SpringMapEditGUI smeGUI;
	private SpringMapEdit sme;
	private AppSettings rs;
	private MapRenderer renderer;
	private FeatureSelectionDialog featureDialog;
	private BrushSelectionDialog brushDialog;
	private TextureSelectionDialog textureDialog;
	private PrefabSelectionDialog prefabDialog;
	
	private EnumMap<Widgets, Control> widgetMap;
	
	private enum Widgets
	{
		L_FOV, SL_FOV,
		L_WATERLEVEL, SL_WATERLEVEL,
		L_BRUSHSTRENGTH, SL_BRUSHSTRENGTH,
		L_BRUSHSIZE, SL_BRUSHSIZE,
		L_MAXHEIGHT, SL_MAXHEIGHT
	}
	
	public SpringMapEditDialog(SpringMapEditGUI mainGUI)
	{
		this.smeGUI = mainGUI;
		this.sme = smeGUI.sme;
		this.rs = smeGUI.as;
		this.renderer = smeGUI.renderer;
		this.featureDialog = new FeatureSelectionDialog(smeGUI, this);
		this.brushDialog = new BrushSelectionDialog(smeGUI, this);
		this.textureDialog = new TextureSelectionDialog(smeGUI, this);
		this.prefabDialog = new PrefabSelectionDialog(smeGUI, this);
		this.widgetMap = new EnumMap<Widgets, Control>(Widgets.class);
	}
	
	private boolean isAdditionalDisplaySupported()
	{
		final boolean[] result = new boolean[1];
		result[0] = true;
		
		//Needs to be in another thread, as apparently one thread with multiple displays does not work too...
		Thread t = new Thread(new Runnable() 
		{
			@Override
			public void run()
			{
				try
				{
					Display test = new Display();
					test.dispose();
				}
				catch (SWTError e)
				{
					result[0] = false;
					System.out.println("ERROR: " + e.getMessage());
					System.out.println("Trying single threaded(single display) approach...");
				}
			}
		});
		t.start();
		
		//Wait for thread to finish...
		boolean notJoined = true;
		while (notJoined)
		{
			try
			{
				t.join();
				notJoined = false;
			}
			catch (InterruptedException e)
			{
				//ignore...
			}
		}
		return result[0];
	}
	
	public void open()
	{
		//Check if we may create a secondary Display:
		isUsingOwnDisplay = isAdditionalDisplaySupported();
		
		if (isUsingOwnDisplay)
		{
			Thread t = new Thread(new Runnable() 
			{	
				@Override
				public void run()
				{
					//Create a new display
					display = new Display();
					
					//Execute main loop
					open_main();
				}
			});
			t.start();
		}
		else
		{
			//Re-use main display
			display = smeGUI.display;
			
			//Execute main loop
			open_main();
		}
	}
	
	public void setAlwaysOnTop(boolean alwaysOnTop)
	{
		rs.dialogAlwaysOnTop = alwaysOnTop;
	}
	
	private void open_main()
	{
		shell = new Shell(display, SWT.DIALOG_TRIM | SWT.RESIZE | (rs.dialogAlwaysOnTop ? SWT.ON_TOP : SWT.NONE));
		shell.setText("Spring Map Editor Setup");
		int width = 370;
		shell.setSize(width, 600);
		shell.setLocation(Toolkit.getDefaultToolkit().getScreenSize().width - width, 0);
		
		//Add close listener
		shell.addShellListener(new ShellAdapter()
		{
			public void shellClosed(ShellEvent e)
			{
				if (quitConfirmed())
				{
					if (!smeGUI.display.isDisposed())
					{
						smeGUI.display.syncExec(new Runnable()
						{
							public void run()
							{
								smeGUI.shell.dispose();
							}
						});
					}
				}
				else
					e.doit = false;
			}
		});			
		
		createDialogArea();
		
		//Open it
		shell.layout();
		shell.setVisible(true);
		
		if (isUsingOwnDisplay)
		{
			//Run message loop
			while (!shell.isDisposed())
			{
				if (!display.readAndDispatch())
					display.sleep();
			}
			
			//Dispose Display
			display.dispose();
		}
	}
	
	private boolean quitConfirmed()
	{
		if (rs.quitWithoutAsking || rs.quietExit)
			return true;
		else
		{
			OkCancelDialog ocd = new OkCancelDialog(shell, "Confirm Exit", "Really Exit? Unsaved changes will be lost.");
			return (ocd.open() == SWT.OK);
		}
	}
	
	private void createDialogArea()
	{
		Menu mainMenuBar = new Menu(shell, SWT.BAR);
		shell.setMenuBar(mainMenuBar);
		
		/////////////////////////////
		// MAP Menu
		/////////////////////////////
		MenuItem menuItem = new MenuItem(mainMenuBar, SWT.CASCADE);
		menuItem.setText("Map");
		Menu menu = new Menu(menuItem);
		menuItem.setMenu(menu);
		
		menuItem = new MenuItem(menu, SWT.PUSH);
		menuItem.setText("New Map");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				NewMapDialog nmd = new NewMapDialog(shell, smeGUI);
				nmd.open();
			}
		});
		
		menuItem = new MenuItem(menu, SWT.SEPARATOR);

		/////////////////////////////
		// LOAD Submenu
		/////////////////////////////
		menuItem = new MenuItem(menu, SWT.CASCADE);
		menuItem.setText("Load");
		Menu subMenu = new Menu(menuItem);
		menuItem.setMenu(subMenu);
		
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Load All Maps");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.OPEN);
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]), renderer }) 
					{
						public void execute(Object[] data2)
						{
							sme.loadAllMaps((File) data[0], (MapRenderer) data[1]);
							((MapRenderer) data[1]).setSpringMapEdit(sme);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Import Spring SM2 Map");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.OPEN);
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]), renderer }) 
					{
						public void execute(Object[] data2)
						{
							sme.loadSM2Map((File) data[0], (MapRenderer) data[1]);
							((MapRenderer) data[1]).setSpringMapEdit(sme);
							
							//Set MaxHeight and Waterlevel after loading
							display.syncExec(new Runnable()
							{
								public void run()
								{
									setWidgetValue(Widgets.SL_MAXHEIGHT, Integer.toString(sme.maxHeight));
									setWidgetValue(Widgets.L_MAXHEIGHT, "MaxHeight: " + sme.maxHeight);
									
									setWidgetValue(Widgets.SL_WATERLEVEL, Integer.toString((int)sme.waterHeight + 1));
									setWidgetValue(Widgets.L_WATERLEVEL, "Waterlevel: " + ((int)sme.waterHeight));
								}
							});
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.SEPARATOR);
		
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Load HeightMap");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.OPEN);
				fd.setFilterExtensions(new String[] { "*.bmp; *.raw; *.png; *.tga; *.tif" });
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]) }) 
					{
						public void execute(Object[] data2)
						{
							boolean ok = true;
							File filename = (File) data[0];
							if (FileHandler.isHandledByBitmap(filename))
							{
								Bitmap bitmap = new Bitmap(filename);
								if (bitmap.requiresSizeInfo())
								{
									InputBox box = new InputBox(smeGUI.shell, "Please enter width of RAW Image", "513");
									int result = box.open();
									if (result > 0)
										ok = bitmap.setSizeInfo(result);
									else
										ok = false;
								}
								if (ok)
								{
									sme.loadDataIntoHeightmap(bitmap);
									renderer.setSpringMapEdit(sme);
								}
							}
							else
							{
								sme.loadDataIntoHeightmap(filename);
								renderer.setSpringMapEdit(sme);
							}
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Load TextureMap");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.OPEN);
				fd.setFilterExtensions(new String[] { "*.bmp" });
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]) }) 
					{
						public void execute(Object[] data2)
						{
							sme.loadDataIntoTexturemap((File) data[0]);
							renderer.invalidateAllBlocks(false, true, false);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Load MetalMap");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.OPEN);
				fd.setFilterExtensions(new String[] { "*.bmp" });
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]) }) 
					{
						public void execute(Object[] data2)
						{
							sme.loadDataIntoMetalmap((File) data[0]);
							renderer.invalidateAllBlocks(false, true, false);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Load FeatureMap");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.OPEN);
				fd.setFilterExtensions(new String[] { "*.fmf" });
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]), renderer }) 
					{
						public void execute(Object[] data2)
						{
							sme.loadDataIntoFeaturemap((File) data[0], (MapRenderer) data[1]);
							renderer.invalidateAllBlocks(false, false, true);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Load TypeMap");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.OPEN);
				fd.setFilterExtensions(new String[] { "*.bmp" });
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]) }) 
					{
						public void execute(Object[] data2)
						{
							sme.loadDataIntoTypemap((File) data[0]);
							renderer.invalidateAllBlocks(false, true, false);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Load VegetationMap");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.OPEN);
				fd.setFilterExtensions(new String[] { "*.bmp" });
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]) }) 
					{
						public void execute(Object[] data2)
						{
							sme.loadDataIntoVegetationmap((File) data[0]);
							renderer.invalidateAllBlocks(false, true, false);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		
		/////////////////////////////
		// SAVE Submenu
		/////////////////////////////
		menuItem = new MenuItem(menu, SWT.CASCADE);
		menuItem.setText("Save");
		subMenu = new Menu(menuItem);
		menuItem.setMenu(subMenu);
		
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Save All Maps");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.SAVE);
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]), renderer }) 
					{
						public void execute(Object[] data2)
						{
							sme.saveAllMaps((File) data[0], (MapRenderer) data[1]);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Export Spring SM2 Map");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.SAVE);
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]), renderer }) 
					{
						public void execute(Object[] data2)
						{
							sme.saveSM2Map((File) data[0], (MapRenderer) data[1]);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.SEPARATOR);
		
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Save HeightMap (8bit)");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.SAVE);
				fd.setFilterExtensions(new String[] { "*.bmp" });
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]) }) 
					{
						public void execute(Object[] data2)
						{
							sme.saveHeightMap((File) data[0], FileFormat.Bitmap8Bit);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
				
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Save HeightMap (RAW 16bit)");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.SAVE);
				fd.setFilterExtensions(new String[] { "*.raw" });
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]) }) 
					{
						public void execute(Object[] data2)
						{
							sme.saveHeightMap((File) data[0], FileFormat.Raw16Bit);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Save HeightMap (RAW 16bit + SimpleHeader)");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.SAVE);
				fd.setFilterExtensions(new String[] { "*.raw" });
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]) }) 
					{
						public void execute(Object[] data2)
						{
							sme.saveHeightMap((File) data[0], FileFormat.Raw16BitSM);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Save HeightMap (PNG 16bit)");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.SAVE);
				fd.setFilterExtensions(new String[] { "*.png" });
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]) }) 
					{
						public void execute(Object[] data2)
						{
							sme.saveHeightMap((File) data[0], FileFormat.PNG16Bit);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Save TextureMap");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.SAVE);
				fd.setFilterExtensions(new String[] { "*.bmp" });
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]) }) 
					{
						public void execute(Object[] data2)
						{
							sme.saveTextureMap((File) data[0]);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Save MetalMap (8bit)");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.SAVE);
				fd.setFilterExtensions(new String[] { "*.bmp" });
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]) }) 
					{
						public void execute(Object[] data2)
						{
							sme.saveMetalMap((File) data[0], false);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Save MetalMap (24bit)");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.SAVE);
				fd.setFilterExtensions(new String[] { "*.bmp" });
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]) }) 
					{
						public void execute(Object[] data2)
						{
							sme.saveMetalMap((File) data[0], true);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Save FeatureMap");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.SAVE);
				fd.setFilterExtensions(new String[] { "*.fmf" });
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]), renderer }) 
					{
						public void execute(Object[] data2)
						{
							sme.saveFeatureMap((File) data[0], (MapRenderer) data[1]);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Save TypeMap (8bit)");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.SAVE);
				fd.setFilterExtensions(new String[] { "*.bmp" });
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]) }) 
					{
						public void execute(Object[] data2)
						{
							sme.saveTypeMap((File) data[0], false);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Save TypeMap (24bit)");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.SAVE);
				fd.setFilterExtensions(new String[] { "*.bmp" });
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]) }) 
					{
						public void execute(Object[] data2)
						{
							sme.saveTypeMap((File) data[0], true);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
			
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Save VegetationMap");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.SAVE);
				fd.setFilterExtensions(new String[] { "*.bmp" });
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]) }) 
					{
						public void execute(Object[] data2)
						{
							sme.saveVegetationMap((File) data[0]);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.PUSH);
		menuItem.setText("Save SlopeMap");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.SAVE);
				fd.setFilterExtensions(new String[] { "*.bmp" });
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]) }) 
					{
						public void execute(Object[] data2)
						{
							sme.saveSlopeMap((File) data[0]);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		/////////////////////
		// MAP Menu Continue
		/////////////////////
		menuItem = new MenuItem(menu, SWT.SEPARATOR);
		
		menuItem = new MenuItem(menu, SWT.PUSH);
		menuItem.setText("Exit");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				if (quitConfirmed())
				{
					Command cmd = new Command(null) 
					{
						public void execute(Object[] data2)
						{
							smeGUI.shell.dispose();
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		/////////////////////////////
		// VIEW Menu
		/////////////////////////////
		menuItem = new MenuItem(mainMenuBar, SWT.CASCADE);
		menuItem.setText("View");
		menu = new Menu(menuItem);
		menuItem.setMenu(menu);
		
		/////////////////////////////
		// View Modes Submenu
		/////////////////////////////
		menuItem = new MenuItem(menu, SWT.CASCADE);
		menuItem.setText("View Modes");
		subMenu = new Menu(menuItem);
		menuItem.setMenu(subMenu);
		
		menuItem = new MenuItem(subMenu, SWT.RADIO);
		menuItem.setText("show Metalmap");
		menuItem.setSelection(rs.mapMode == MapMode.MetalMap);
		menuItem.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				Command cmd = new Command(new Object[] { ((MenuItem)e.widget).getSelection() }) 
				{
					public void execute(Object[] data2)
					{
						if ((Boolean) data[0]) rs.mapMode = MapMode.MetalMap;
						renderer.invalidateAllBlocks(false, true, false);
					}
				};
				smeGUI.messageQueue.offer(cmd);
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.RADIO);
		menuItem.setText("show Typemap");
		menuItem.setSelection(rs.mapMode == MapMode.TypeMap);
		menuItem.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				Command cmd = new Command(new Object[] { ((MenuItem)e.widget).getSelection() }) 
				{
					public void execute(Object[] data2)
					{
						if ((Boolean) data[0]) rs.mapMode = MapMode.TypeMap;
						renderer.invalidateAllBlocks(false, true, false);
					}
				};
				smeGUI.messageQueue.offer(cmd);
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.RADIO);
		menuItem.setText("show Vegetationmap");
		menuItem.setSelection(rs.mapMode == MapMode.VegetationMap);
		menuItem.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				Command cmd = new Command(new Object[] { ((MenuItem)e.widget).getSelection() }) 
				{
					public void execute(Object[] data2)
					{
						if ((Boolean) data[0]) rs.mapMode = MapMode.VegetationMap;
						renderer.invalidateAllBlocks(false, true, false);
					}
				};
				smeGUI.messageQueue.offer(cmd);
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.RADIO);
		menuItem.setText("show Slopemap");
		menuItem.setSelection(rs.mapMode == MapMode.SlopeMap);
		menuItem.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				Command cmd = new Command(new Object[] { ((MenuItem)e.widget).getSelection() }) 
				{
					public void execute(Object[] data2)
					{
						if ((Boolean) data[0]) rs.mapMode = MapMode.SlopeMap;
						renderer.invalidateAllBlocks(false, true, false);
					}
				};
				smeGUI.messageQueue.offer(cmd);
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.RADIO);
		menuItem.setText("show Texturemap");
		menuItem.setSelection(rs.mapMode == MapMode.TextureMap);
		menuItem.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				Command cmd = new Command(new Object[] { ((MenuItem)e.widget).getSelection() }) 
				{
					public void execute(Object[] data2)
					{
						if ((Boolean) data[0]) rs.mapMode = MapMode.TextureMap;
						renderer.invalidateAllBlocks(false, true, false);
					}
				};
				smeGUI.messageQueue.offer(cmd);
			}
		});
		
		////////////////////
		//VIEW Menu Continue
		////////////////////
		menuItem = new MenuItem(menu, SWT.CHECK);
		menuItem.setText("Blend in Texturemap");
		menuItem.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				Command cmd = new Command(new Object[] { ((MenuItem)e.widget).getSelection() }) 
				{
					public void execute(Object[] data2)
					{
						rs.blendTextureMap = (Boolean) data[0];
						renderer.invalidateAllBlocks(false, true, false);
					}
				};
				smeGUI.messageQueue.offer(cmd);
			}
		});
		
		/////////////////////////////
		// TexGen Menu
		/////////////////////////////
		menuItem = new MenuItem(mainMenuBar, SWT.CASCADE);
		menuItem.setText("TexGen");
		menu = new Menu(menuItem);
		menuItem.setMenu(menu);
		
		menuItem = new MenuItem(menu, SWT.PUSH);
		menuItem.setText("Load TexGen settings");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				
				FileDialog fd = new FileDialog(shell, SWT.OPEN);
				fd.setFilterExtensions(new String[] { "*.tdf" });
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]) }) 
					{
						public void execute(Object[] data2)
						{
							sme.mes.setTextureGeneratorSetupByFile((File) data[0]);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		menuItem = new MenuItem(menu, SWT.SEPARATOR);
		
		menuItem = new MenuItem(menu, SWT.PUSH);
		menuItem.setText("TexGen whole map");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				Command cmd = new Command(null) 
				{
					public void execute(Object[] data2)
					{
						smeGUI.sme.genColorsByHeight(0, 0, null);
						smeGUI.renderer.invalidateAllBlocks(false, true, false);
					}
				};
				smeGUI.messageQueue.offer(cmd);
			}
		});
		
		/////////////////////////////
		// Erosion Menu
		/////////////////////////////
		menuItem = new MenuItem(mainMenuBar, SWT.CASCADE);
		menuItem.setText("Erosion");
		menu = new Menu(menuItem);
		menuItem.setMenu(menu);
		
		menuItem = new MenuItem(menu, SWT.PUSH);
		menuItem.setText("Load erosion settings");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				
				FileDialog fd = new FileDialog(shell, SWT.OPEN);
				fd.setFilterExtensions(new String[] { "*.tdf" });
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]) }) 
					{
						public void execute(Object[] data2)
						{
							sme.mes.setErosionSetupByFile((File) data[0]);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		menuItem = new MenuItem(menu, SWT.PUSH);
		menuItem.setText("Save erosion settings");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog fd = new FileDialog(shell, SWT.SAVE);
				if (fd.open() != null)
				{
					Command cmd = new Command(new Object[] { new File(fd.getFilterPath(), fd.getFileNames()[0]) }) 
					{
						public void execute(Object[] data2)
						{
							String path = ((File) data[0]).getAbsolutePath();
							String ext = ".tdf";
							if (!path.endsWith(ext))
								path = path + ext;
							sme.mes.getErosionSetup().saveToFile(new File(path));
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		menuItem = new MenuItem(menu, SWT.PUSH);
		menuItem.setText("Erosion Settings");
		menuItem.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				ErosionSettingsDialog esd = new ErosionSettingsDialog(shell, smeGUI);
				esd.open();
			}
		});
		
		menuItem = new MenuItem(menu, SWT.SEPARATOR);
		
		menuItem = new MenuItem(menu, SWT.PUSH);
		menuItem.setText("Wet erode whole map");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				smeGUI.messageQueue.offer(new Edit_Erode_Heightmap_Wet(smeGUI));
			}
		});
		
		menuItem = new MenuItem(menu, SWT.PUSH);
		menuItem.setText("Dry erode whole map");
		menuItem.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				smeGUI.messageQueue.offer(new Edit_Erode_Heightmap_Dry(smeGUI));
			}
		});
		
		/////////////////////////////
		// SETTINGS Menu
		/////////////////////////////
		menuItem = new MenuItem(mainMenuBar, SWT.CASCADE);
		menuItem.setText("Settings");
		menu = new Menu(menuItem);
		menuItem.setMenu(menu);
		
		menuItem = new MenuItem(menu, SWT.PUSH);
		menuItem.setText("Render Settings");
		menuItem.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				RenderSettingsDialog rsd = new RenderSettingsDialog(shell, smeGUI);
				rsd.open();
			}
		});
		
		menuItem = new MenuItem(menu, SWT.PUSH);
		menuItem.setText("SM2 Compile Settings");
		menuItem.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				CompileSettingsDialog csd = new CompileSettingsDialog(shell, smeGUI);
				csd.open();
			}
		});
		
		//////////////////////////////
		////Quicksave submenu
		//////////////////////////////
		menuItem = new MenuItem(menu, SWT.CASCADE);
		menuItem.setText("Quicksave");
		subMenu = new Menu(menuItem);
		menuItem.setMenu(subMenu);
		
		menuItem = new MenuItem(subMenu, SWT.CHECK);
		menuItem.setSelection(rs.quicksave_compress);
		menuItem.setText("Compress Quicksaves (slower, saves RAM)");
		menuItem.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				Command cmd = new Command(new Object[] { ((MenuItem)e.widget).getSelection() }) 
				{
					public void execute(Object[] data2)
					{
						rs.quicksave_compress = (Boolean) data[0];
					}
				};
				smeGUI.messageQueue.offer(cmd);
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.CHECK);
		menuItem.setSelection(rs.quicksave_heightmap);
		menuItem.setText("store Heightmap");
		menuItem.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				Command cmd = new Command(new Object[] { ((MenuItem)e.widget).getSelection() }) 
				{
					public void execute(Object[] data2)
					{
						rs.quicksave_heightmap = (Boolean) data[0];
					}
				};
				smeGUI.messageQueue.offer(cmd);
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.CHECK);
		menuItem.setSelection(rs.quicksave_texturemap);
		menuItem.setText("store Texturemap");
		menuItem.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				Command cmd = new Command(new Object[] { ((MenuItem)e.widget).getSelection() }) 
				{
					public void execute(Object[] data2)
					{
						rs.quicksave_texturemap = (Boolean) data[0];
					}
				};
				smeGUI.messageQueue.offer(cmd);
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.CHECK);
		menuItem.setSelection(rs.quicksave_metalmap);
		menuItem.setText("store Metalmap");
		menuItem.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				Command cmd = new Command(new Object[] { ((MenuItem)e.widget).getSelection() }) 
				{
					public void execute(Object[] data2)
					{
						rs.quicksave_metalmap = (Boolean) data[0];
					}
				};
				smeGUI.messageQueue.offer(cmd);
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.CHECK);
		menuItem.setSelection(rs.quicksave_typemap);
		menuItem.setText("store Typemap");
		menuItem.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				Command cmd = new Command(new Object[] { ((MenuItem)e.widget).getSelection() }) 
				{
					public void execute(Object[] data2)
					{
						rs.quicksave_typemap = (Boolean) data[0];
					}
				};
				smeGUI.messageQueue.offer(cmd);
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.CHECK);
		menuItem.setSelection(rs.quicksave_vegetationmap);
		menuItem.setText("store Vegetationmap");
		menuItem.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				Command cmd = new Command(new Object[] { ((MenuItem)e.widget).getSelection() }) 
				{
					public void execute(Object[] data2)
					{
						rs.quicksave_vegetationmap = (Boolean) data[0];
					}
				};
				smeGUI.messageQueue.offer(cmd);
			}
		});
		
		menuItem = new MenuItem(subMenu, SWT.CHECK);
		menuItem.setSelection(rs.quicksave_featuremap);
		menuItem.setText("store Featuremap");
		menuItem.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				Command cmd = new Command(new Object[] { ((MenuItem)e.widget).getSelection() }) 
				{
					public void execute(Object[] data2)
					{
						rs.quicksave_featuremap = (Boolean) data[0];
					}
				};
				smeGUI.messageQueue.offer(cmd);
			}
		});
		
		///////////////////////////
		//SETTINGS Menu Continue
		///////////////////////////
		
		menuItem = new MenuItem(menu, SWT.CHECK);
		menuItem.setSelection(rs.quitWithoutAsking);
		menuItem.setText("Quit without asking");
		menuItem.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				Command cmd = new Command(new Object[] { ((MenuItem)e.widget).getSelection() }) 
				{
					public void execute(Object[] data2)
					{
						rs.quitWithoutAsking = (Boolean) data[0];
					}
				};
				smeGUI.messageQueue.offer(cmd);
			}
		});
		
		menuItem = new MenuItem(menu, SWT.CHECK);
		menuItem.setSelection(rs.dialogAlwaysOnTop);
		menuItem.setText("Dialog always on top (requires restart)");
		menuItem.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				Command cmd = new Command(new Object[] { ((MenuItem)e.widget).getSelection() }) 
				{
					public void execute(Object[] data2)
					{
						rs.dialogAlwaysOnTop = (Boolean) data[0];
					}
				};
				smeGUI.messageQueue.offer(cmd);
			}
		});
		
		
		////Layout
		shell.setLayout(new GridLayout(4, false));
		
		////Controls
		addSlider(Widgets.L_FOV, Widgets.SL_FOV, shell, "FOV: " + (int)rs.fov, (int)rs.fov, 10, 180, 
				new Command(null) { public void execute(Object[] data2)
				{
					rs.fov = (Integer) data2[0];
					renderer.camViewChangedNotify();
				}},
				new Getter(null) { public Object getValue(Object[] data2)
				{
					return "FOV: " + (Integer) data2[0];
				}});
		
		final Slider wl = addSlider(Widgets.L_WATERLEVEL, Widgets.SL_WATERLEVEL, shell, "Waterlevel: " + (int)sme.waterHeight, (int)sme.waterHeight, 0, sme.maxHeight + 2, 
				new Command(null) { public void execute(Object[] data2)
				{
					sme.waterHeight = ((Integer) data2[0]) - 1;
					renderer.camViewChangedNotify();
				}},
				new Getter(null) { public Object getValue(Object[] data2)
				{
					return "Waterlevel: " + (((Integer) data2[0]) - 1);
				}});
		
		addSlider(Widgets.L_BRUSHSTRENGTH, Widgets.SL_BRUSHSTRENGTH, shell, "Brush Strength: " + (int)sme.mes.activeBrush.getStrengthInt(), (int)sme.mes.activeBrush.getStrengthInt(), 1, 1000, 
				new Command(null) { public void execute(Object[] data2)
				{
					sme.mes.activeBrush.setStrengthInt((Integer) data2[0]);
				}},
				new Getter(null) { public Object getValue(Object[] data2)
				{
					return "Brush Strength: " + (Integer) data2[0];
				}});
		
		addSlider(Widgets.L_BRUSHSIZE, Widgets.SL_BRUSHSIZE, shell, "Brush Size: " + sme.mes.activeBrush.width, sme.mes.activeBrush.width, 1, 1000,
				new Command(null) { public void execute(Object[] data2)
				{
					sme.mes.activeBrush.setSize((Integer) data2[0], (Integer) data2[0]);
				}},
				new Getter(null) { public Object getValue(Object[] data2)
				{
					return "Brush Size: " + (Integer) data2[0];
				}});
		
		addSlider(Widgets.L_MAXHEIGHT, Widgets.SL_MAXHEIGHT, shell, "MaxHeight: " + (int)sme.maxHeight, (int)sme.maxHeight, 10, 500, 
				new Command(null) { public void execute(Object[] data2)
				{
					sme.maxHeight = (Integer) data2[0];
					renderer.invalidateAllBlocks(true, false, false);
				}},
				new Getter(null) { public Object getValue(Object[] data2)
				{
					return "MaxHeight: " + (Integer) data2[0];
				}}
				).addSelectionListener(new SelectionAdapter() 
				{
					public void widgetSelected(SelectionEvent e)
					{
						//adjust waterlevel maxima
						wl.setMaximum(((Slider)e.widget).getSelection() + 3);
						if (wl.getMaximum() < wl.getSelection())
						{
							wl.setSelection(wl.getMaximum() - 1);
							sme.waterHeight = wl.getSelection();
						}
					}
				});
		
		Group bg = addRadioGroup(shell, "Brushmode", sme.mes.getBrushMode().ordinal(), 4, 7, new String[] { "Height", "Texture", "Metal", "Type", "Vegetation", "Feature", "Prefab" }, new Command[] {
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0])
				{
					sme.mes.setBrushMode(BrushMode.Height);
					updateWidgets();
				}
			}},
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0]) 
				{
					sme.mes.setBrushMode(BrushMode.Texture);
					updateWidgets();
				}
			}},
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0])
				{
					sme.mes.setBrushMode(BrushMode.Metal);
					updateWidgets();
				}
			}},
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0])
				{
					sme.mes.setBrushMode(BrushMode.Type);
					updateWidgets();
				}
			}},
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0])
				{
					sme.mes.setBrushMode(BrushMode.Vegetation);
					updateWidgets();
				}
			}},
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0])
				{
					sme.mes.setBrushMode(BrushMode.Feature);
					updateWidgets();
				}
			}},
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0])
				{
					sme.mes.setBrushMode(BrushMode.Prefab);
					updateWidgets();
				}
			}}
		});
		Button b = new Button(bg, SWT.PUSH);
		b.setText("Select Brush");
		b.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				brushDialog.open();
			}
		});
		
		b = new Button(bg, SWT.PUSH);
		b.setText("Rotate CCW");
		b.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				sme.mes.activeBrush.rotate(true);
				updateWidgets();
			}
		});
		
		b = new Button(bg, SWT.PUSH);
		b.setText("Rotate CW");
		b.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				sme.mes.activeBrush.rotate(false);
				updateWidgets();
			}
		});
		
		b = new Button(bg, SWT.PUSH);
		b.setText("Mirror horiz");
		b.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				sme.mes.activeBrush.mirror(true);
				updateWidgets();
			}
		});
		
		b = new Button(bg, SWT.PUSH);
		b.setText("Mirror vert");
		b.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				sme.mes.activeBrush.mirror(false);
				updateWidgets();
			}
		});
		
		addRadioGroup(shell, "Heightmode", sme.mes.getHeightBrush().mode.ordinal(), 3, 5, new String[] { "Raise", "Smooth", "Set", "Erode", "Randomize" }, new Command[] {
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0])
				{
					sme.mes.getHeightBrush().mode = HeightMode.Raise;
					updateWidgets();
				}
			}},
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0])
				{
					sme.mes.getHeightBrush().mode = HeightMode.Smooth;
					updateWidgets();
				}
			}},
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0])
				{
					sme.mes.getHeightBrush().mode = HeightMode.Set;
					updateWidgets();
				}
			}},
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0])
				{
					sme.mes.getHeightBrush().mode = HeightMode.Erode;
					updateWidgets();
				}
			}},
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0])
				{
					sme.mes.getHeightBrush().mode = HeightMode.Randomize;
					updateWidgets();
				}
			}}
		});
		
		Group pg = addRadioGroup(shell, "Texturemode", sme.mes.getTextureBrush().mode.ordinal(), 4, 5, new String[] { "Set", "Add", "Blend", "Stamp", "TexGen" }, new Command[] {
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0])
				{
					sme.mes.getTextureBrush().mode = TextureMode.Set;
					updateWidgets();
				}
			}},
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0])
				{
					sme.mes.getTextureBrush().mode = TextureMode.Add;
					updateWidgets();
				}
			}},
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0])
				{
					sme.mes.getTextureBrush().mode = TextureMode.Blend;
					updateWidgets();
				}
			}},
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0])
				{
					sme.mes.getTextureBrush().mode = TextureMode.Stamp;
					updateWidgets();
				}
			}},
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0])
				{
					sme.mes.getTextureBrush().mode = TextureMode.TexGen;
					updateWidgets();
				}
			}}
		});
		
		b = new Button(pg, SWT.PUSH);
		b.setText("Select Texture");
		b.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				textureDialog.open();
			}
		});
		
		b = new Button(pg, SWT.PUSH);
		b.setText("Select Color");
		b.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				ColorDialog cd = new ColorDialog(shell);
				if (cd.open() != null)
				{
					Command cmd = new Command(new Object[] { cd.getRGB().red, cd.getRGB().green, cd.getRGB().blue })
					{
						public void execute(Object[] data2)
						{
							sme.mes.getTextureBrush().setTextureToColor((Integer)data[0], (Integer)data[1], (Integer)data[2]);
						}
					};
					smeGUI.messageQueue.offer(cmd);
				}
			}
		});
		
		Group fg = addRadioGroup(shell, "Featuremode", sme.mes.getFeatureBrush().mode.ordinal(), 3, 3, new String[] { "Add", "Rotate", "Rotate Same Random" }, new Command[] {
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0])
				{
					sme.mes.getFeatureBrush().mode = FeatureMode.Add;
					updateWidgets();
				}
			}},
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0])
				{
					sme.mes.getFeatureBrush().mode = FeatureMode.Rotate;
					updateWidgets();
				}
			}},
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0])
				{
					sme.mes.getFeatureBrush().mode = FeatureMode.RotateSameRandom;
					updateWidgets();
				}
			}}
		});
		
		b = new Button(fg, SWT.PUSH);
		b.setText("Select Feature");
		b.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				featureDialog.open();
			}
		});
		
		Group pfg = addRadioGroup(shell, "Prefabmode", sme.mes.getPrefabBrush().mode.ordinal(), 3, 2, new String[] { "Set", "Add" }, new Command[] {
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0])
				{
					sme.mes.getPrefabBrush().mode = PrefabMode.Set;
					updateWidgets();
				}
			}},
			new Command(null) { public void execute(Object[] data2)
			{
				if ((Boolean)data2[0])
				{
					sme.mes.getPrefabBrush().mode = PrefabMode.Add;
					updateWidgets();
				}
			}}
		});
		
		b = new Button(pfg, SWT.PUSH);
		b.setText("Select Prefab");
		b.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				prefabDialog.open();
			}
		});
		
		b = new Button(pfg, SWT.PUSH);
		b.setText("Align 1x1 Prefab");
		b.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				sme.mes.brushAlign.set(sme.mes.getPrefabBrush().width - 1, sme.mes.getPrefabBrush().height - 1);
				sme.mes.brushHeightAlign = sme.mes.getPrefabBrush().prefab.heightZ;
			}
		});
		
		b = new Button(pfg, SWT.PUSH);
		b.setText("UnAlign Prefab");
		b.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(SelectionEvent e)
			{
				sme.mes.brushAlign.set(1, 1);
				sme.mes.brushHeightAlign = 0;
			}
		});
	}

	/**
	 * Synchronized update method
	 */
	public void updateWidgets()
	{
		display.syncExec(new Runnable()
		{
			public void run()
			{
				//1. Get active Brush
				Brush brush = sme.mes.activeBrush;
				
				//2. Set widgets to brush values
				setWidgetValue(Widgets.SL_BRUSHSIZE, Integer.toString(brush.width));
				setWidgetValue(Widgets.L_BRUSHSIZE, "Brush Size: " + brush.width);
				
				setWidgetValue(Widgets.SL_BRUSHSTRENGTH, Integer.toString(brush.getStrengthInt()));
				setWidgetValue(Widgets.L_BRUSHSTRENGTH, "Brush Strength: " + brush.getStrengthInt());
				
				//TODO Update Brush/Texture Windows too
			}
		});
	}
	
	private void setWidgetValue(Widgets widget, String value)
	{
		Control c = widgetMap.get(widget);
		if (c != null)
		{
			if (c.getClass().equals(Label.class))
			{
				((Label)c).setText(value);
			}
			else if (c.getClass().equals(Slider.class))
			{
				((Slider)c).setSelection(Integer.parseInt(value));
			}
		}
	}
	
	private Slider addSlider(Widgets wLabel, Widgets wSlider, Composite parent, String initialText, int initialValue, int min, int max, final Command cmd, final Getter getText)
	{
		final Label label = new Label(parent, SWT.HORIZONTAL);
		GridData gd = new GridData(GridData.FILL, GridData.BEGINNING, false, false, 1, 1);
		gd.widthHint = 100;
		label.setLayoutData(gd);
		label.setText(initialText);
		
		Slider sl = new Slider(parent, SWT.HORIZONTAL);
		sl.setMinimum(min);
		sl.setMaximum(max + 1);
		sl.setThumb(1);
		sl.setSelection(initialValue);
		gd = new GridData(GridData.FILL, GridData.BEGINNING, true, false, 3, 1);
		sl.setLayoutData(gd);
		sl.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				int value = ((Slider)e.widget).getSelection();
				label.setText((String)getText.getValue(new Object[] { value }));
				
				Command newCmd = new Command(new Object[] { value })
				{
					public void execute(Object[] data2)
					{
						cmd.execute(new Object[] { data[0] });
					}
				};
				smeGUI.messageQueue.offer(newCmd);
			}
		});
		
		widgetMap.put(wLabel, label);
		widgetMap.put(wSlider, sl);
		
		return sl;
	}
	
	private Group addRadioGroup(Composite parent, String caption, int initialSelection, int columns, int count, String[] names, final Command[] cmds)
	{
		Group group = new Group(shell, SWT.SHADOW_NONE);
		group.setText(caption);
		GridData gd = new GridData(GridData.FILL, GridData.BEGINNING, true, false, 4, 1);
		group.setLayoutData(gd);
		group.setLayout(new GridLayout(columns, false));
		
		for (int i = 0; i < count; i++)
		{
			Button b = new Button(group, SWT.RADIO);
			b.setText(names[i]);
			b.setLayoutData(new GridData(GridData.FILL, GridData.BEGINNING, true, false, 1, 1));
			
			if (i == initialSelection) b.setSelection(true);
			final int fi = i;
			
			b.addSelectionListener(new SelectionAdapter()
			{
				public void widgetSelected(SelectionEvent e)
				{
					boolean value = ((Button)e.widget).getSelection();
					
					Command newCmd = new Command(new Object[] { value, cmds[fi] })
					{
						public void execute(Object[] data2)
						{
							((Command) data[1]).execute(new Object[] { data[0] });
						}
					};
					smeGUI.messageQueue.offer(newCmd);
				}
			});
		}
		
		return group;
	}
	
	public void dispose()
	{
		//Check if we use own display&thread. If yes, close it. If no, do nothing (we rely on parent display)
		if (isUsingOwnDisplay)
		{
			if (!display.isDisposed())
			{
				display.syncExec(new Runnable()
				{
					public void run()
					{
						if (!shell.isDisposed())
							shell.dispose();
					}
				});
			}
		}
	}
	
	public void animate()
	{
		featureDialog.animate();
		prefabDialog.animate();
	}
}
