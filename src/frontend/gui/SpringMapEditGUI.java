/*
 * SpringMapEdit -- A 3D map editor for the Spring engine
 *
 * Copyright (C) 2008-2009  Heiko Schmitt <heikos23@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
/**
 * SpringMapEditGUI.java 
 * Created on 03.07.2008
 * by Heiko Schmitt
 */
package frontend.gui;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;
import java.io.File;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.LockSupport;

import javax.media.opengl.GLContext;
import javax.media.opengl.GLDrawableFactory;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.opengl.GLCanvas;
import org.eclipse.swt.opengl.GLData;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import backend.SpringMapEdit;
import backend.math.Vector3;
import backend.math.Vector3Math;
import frontend.keybinding.KeyMapper;
import frontend.render.MapRenderer;
import frontend.render.AppSettings;

/**
 * @author Heiko Schmitt
 *
 */
public class SpringMapEditGUI
{
	public Display display;
	public Shell shell;
	
	private Cursor cursorDefault;
	private Cursor cursorCross;
	
	public SpringMapEdit sme;
	
	public GLContext glContext;
	public GLCanvas glCanvas;
	public AppSettings as;
	public MapRenderer renderer;
	private SpringMapEditDialog setupDialog;
	
	public KeyMapper keyMap;
	public boolean[] holdableKeys;
	
	public boolean lockCameraY;
	
	private Point mousePos, mousePosOld, mousePosCenter;
		
	public Queue<Command> glMessageQueue;
	public Queue<Command> messageQueue;
	
	public enum HoldableKeys
	{
		SHIFT,
		CTRL,
		ALT,
		ARROW_LEFT,
		ARROW_RIGHT,
		ARROW_DOWN,
		ARROW_UP,
		MOUSE_1,
		MOUSE_2,
		MOUSE_3,
		
		LAST
	}
	private static final int KEYCOUNT = HoldableKeys.LAST.ordinal();
			
	/**
	 * @throws HeadlessException
	 */
	public SpringMapEditGUI(AppSettings settings)
	{
		this.as = settings;
		
		this.display = new Display();
		this.shell = new Shell(display);
		
		//Show Logo Dialog
		try
		{
			new LogoDialog(shell, 3000, new File("textures/logo.png"));
		}
		catch (Exception e)
		{
			//ignore...
		}
		
		GLData data = new GLData ();
		data.doubleBuffer = true;
		glCanvas = new GLCanvas(shell, SWT.NO_BACKGROUND, data);
		glCanvas.setSize(as.displayWidth, as.displayHeight);
		
		cursorDefault = new Cursor(display, SWT.CURSOR_ARROW);
		cursorCross = new Cursor(display, SWT.CURSOR_CROSS);

		this.messageQueue = new ConcurrentLinkedQueue<Command>();
		this.glMessageQueue = new ConcurrentLinkedQueue<Command>();
		
		this.holdableKeys = new boolean[KEYCOUNT];
		for (int i = 0; i < KEYCOUNT; i++)
		{
			this.holdableKeys[i] = false;
		}
		mousePosCenter = new Point(as.displayWidth / 2, as.displayHeight / 2);
		mousePos = new Point(as.displayWidth / 2, as.displayHeight / 2);
		this.keyMap = new KeyMapper(this);
		keyMap.loadFromFile(new File("config/keys.cfg"));
		shell.setText("Spring Map Editor");
		
		shell.addShellListener(new ShellAdapter() 
		{
			public void shellActivated(ShellEvent e)
			{
				super.shellActivated(e);
				glCanvas.forceFocus();
			}
			
			@Override
			public void shellClosed(ShellEvent e)
			{
				if ((!as.quitWithoutAsking) && (!as.quietExit))
				{
					OkCancelDialog ocd = new OkCancelDialog(shell, "Confirm Exit", "Really Exit? Unsaved changes will be lost.");
					if (ocd.open() != SWT.OK)
						e.doit = false;
				}
			}
		});
		
		shell.addListener(SWT.Resize, new Listener() 
		{
			public void handleEvent(Event event)
			{
				setMouseCenter();
				if (glCanvas != null)
					glCanvas.setSize(shell.getClientArea().width, shell.getClientArea().height);
			}
		});
		
		shell.addListener(SWT.Move, new Listener() 
		{
			public void handleEvent(Event event)
			{
				setMouseCenter();
			}
		});
		
		glCanvas.addListener(SWT.Resize, new Listener()
		{
			public void handleEvent(Event event)
			{
				if (renderer != null)
				{
					Command cmd = new Command(new Object[] { shell.getClientArea().width, shell.getClientArea().height }) 
					{
						public void execute(Object[] data2)
						{
							renderer.reshape((Integer)data[0], (Integer)data[1]);
						}
					};
					glMessageQueue.offer(cmd);
				}
			}
		});
		
		glCanvas.addKeyListener(new KeyAdapter() 
		{
			public void keyPressed(KeyEvent e)
			{
				Command cmd = keyMap.getCommandByKeyCode(true, e.keyCode, holdableKeys[HoldableKeys.SHIFT.ordinal()], holdableKeys[HoldableKeys.CTRL.ordinal()], holdableKeys[HoldableKeys.ALT.ordinal()]);
				if (cmd != null)
				{
					messageQueue.offer(cmd);
				}
			}
			
			public void keyReleased(KeyEvent e)
			{
				super.keyReleased(e);
				
				Command cmd = keyMap.getCommandByKeyCode(false, e.keyCode, holdableKeys[HoldableKeys.SHIFT.ordinal()], holdableKeys[HoldableKeys.CTRL.ordinal()], holdableKeys[HoldableKeys.ALT.ordinal()]);
				if (cmd != null)
				{
					messageQueue.offer(cmd);
				}
			}
		});
		
		glCanvas.addMouseListener(new MouseAdapter() 
		{
			public void mouseDown(MouseEvent e)
			{
				Command cmd = keyMap.getCommandByKeyCode(true, e.button, holdableKeys[HoldableKeys.SHIFT.ordinal()], holdableKeys[HoldableKeys.CTRL.ordinal()], holdableKeys[HoldableKeys.ALT.ordinal()]);
				if (cmd != null)
				{
					messageQueue.offer(cmd);
				}
			}
			public void mouseUp(MouseEvent e)
			{
				Command cmd = keyMap.getCommandByKeyCode(false, e.button, holdableKeys[HoldableKeys.SHIFT.ordinal()], holdableKeys[HoldableKeys.CTRL.ordinal()], holdableKeys[HoldableKeys.ALT.ordinal()]);
				if (cmd != null)
				{
					messageQueue.offer(cmd);
				}
			}
		});
		
		glCanvas.addMouseMoveListener(new MouseMoveListener() 
		{
			public void mouseMove(MouseEvent e)
			{
				Command cmd = new Command(new Object[] { e.x, e.y }) 
				{
					public void execute(Object[] data2)
					{
						mousePos = new Point((Integer)data[0], (Integer)data[1]);
					}
				};
				messageQueue.offer(cmd);
			}
		});
		
		this.sme = new SpringMapEdit(as.initialMapWidth, as.initialMapHeight, null, as); //spring units... 8x8 equals 512x512
		
		//Setup initial position
		as.cameraPosition.camX = -50;
		as.cameraPosition.camY = 200;
		as.cameraPosition.camZ = -50;
		as.cameraPosition.camRotX = -30;
		as.cameraPosition.camRotY = 225;
		as.cameraPosition.camRotZ = 0;
		
		this.renderer = new MapRenderer(sme, as);

		glCanvas.setCurrent();
		glContext = GLDrawableFactory.getFactory().createExternalGLContext();
		glContext.makeCurrent();
		renderer.init(glContext.getGL());
		glCanvas.addListener(SWT.Resize, new Listener()
		{
			public void handleEvent(Event event)
			{
				Command cmd = new Command(null)
				{
					public void execute(Object[] data2)
					{
						renderer.reshape(glCanvas.getSize().x, glCanvas.getSize().y);
					}
				};
				glMessageQueue.offer(cmd);
			}
		});
		glContext.release();

		shell.setLocation(0, 0);
		shell.pack();
		
		this.setupDialog = new SpringMapEditDialog(this);
	}
		
	public void toggleMouseLook()
	{
		as.mouseLook = !as.mouseLook;
		
		if (as.mouseLook)
		{
			shell.setCursor(cursorCross);
			try
			{
				Robot r = new Robot();
				r.mouseMove(mousePosCenter.x, mousePosCenter.y);
			}
			catch (AWTException e) 
			{
			}
		}	
		else
		{
			shell.setCursor(cursorDefault);
		}
	}
	
	private void setMouseCenter()
	{
		int x = shell.getLocation().x;
		int y = shell.getLocation().y;
		int w = shell.getSize().x / 2;
		int h = shell.getSize().y / 2;
		mousePosCenter = new Point(w + x, h + y);
	}
	
	public void run()
	{
		//Show Windows
		if (!as.batchMode)
		{
			shell.setVisible(true);
			setupDialog.open();
		}
		
		//Get Focus into this window
		//shell.forceActive();
		
		final FPSMeter fps = new FPSMeter();
		int ticksSinceLastRender = 0;
		boolean makeTickThisLoop = false;
		final int worldFPS = 33; //set wanted fps to 33 FPS
		final long animationTimeStep = 1000000000L / worldFPS;
		long lastAnimation = System.nanoTime();
		long sleepTime;
		final long minSleepTime = 16000000L; //set to minimum Thread.sleep granularity
		while (!shell.isDisposed())
		{
			/* Make world animation/step every 1/50 second */
			makeTickThisLoop = (System.nanoTime() - lastAnimation) > animationTimeStep;
			if (makeTickThisLoop) 
			{
				//Set time of last animation
				lastAnimation = lastAnimation + animationTimeStep;
				
				//Animation
				as.time = as.time + 0.002f;
				as.gameFrame++;
				
				float curMoveSpeed = (holdableKeys[HoldableKeys.SHIFT.ordinal()] ? as.fastSpeed : as.normalSpeed);
				curMoveSpeed = (holdableKeys[HoldableKeys.CTRL.ordinal()] ? as.slowSpeed : curMoveSpeed);
				
				// Handle Camera
				if (holdableKeys[HoldableKeys.ARROW_LEFT.ordinal()]) //Links
				{
					as.cameraPosition.camX = as.cameraPosition.camX - (curMoveSpeed * (float)Math.cos(-as.cameraPosition.camRotY * Math.PI / 180));
					as.cameraPosition.camZ = as.cameraPosition.camZ - (curMoveSpeed * (float)Math.sin(-as.cameraPosition.camRotY * Math.PI / 180));
					renderer.camPosChangedNotify();
				}
				if (holdableKeys[HoldableKeys.ARROW_RIGHT.ordinal()]) //Rechts
				{
					as.cameraPosition.camX = as.cameraPosition.camX + (curMoveSpeed * (float)Math.cos(-as.cameraPosition.camRotY * Math.PI / 180));
					as.cameraPosition.camZ = as.cameraPosition.camZ + (curMoveSpeed * (float)Math.sin(-as.cameraPosition.camRotY * Math.PI / 180));
					renderer.camPosChangedNotify();
				}
				if (holdableKeys[HoldableKeys.ARROW_UP.ordinal()]) //Vor
				{
					double upDownAmount = Math.cos(as.cameraPosition.camRotX * Math.PI / 180);
					if (lockCameraY)
						upDownAmount = 1;
					else
						as.cameraPosition.camY = as.cameraPosition.camY + (curMoveSpeed * (float)Math.sin(as.cameraPosition.camRotX * Math.PI / 180));
					as.cameraPosition.camX = as.cameraPosition.camX + (float)(Math.abs(upDownAmount) * curMoveSpeed * Math.sin(-as.cameraPosition.camRotY * Math.PI / 180));
					as.cameraPosition.camZ = as.cameraPosition.camZ - (float)(Math.abs(upDownAmount) * curMoveSpeed * Math.cos(-as.cameraPosition.camRotY * Math.PI / 180));
					renderer.camPosChangedNotify();
				}
				if (holdableKeys[HoldableKeys.ARROW_DOWN.ordinal()]) //Zurueck
				{
					double upDownAmount = Math.cos(as.cameraPosition.camRotX * Math.PI / 180);
					if (lockCameraY)
						upDownAmount = 1;
					else
						as.cameraPosition.camY = as.cameraPosition.camY - (curMoveSpeed * (float)Math.sin(as.cameraPosition.camRotX * Math.PI / 180));
					as.cameraPosition.camX = as.cameraPosition.camX - (float)(Math.abs(upDownAmount) * curMoveSpeed * Math.sin(-as.cameraPosition.camRotY * Math.PI / 180));
					as.cameraPosition.camZ = as.cameraPosition.camZ + (float)(Math.abs(upDownAmount) * curMoveSpeed * Math.cos(-as.cameraPosition.camRotY * Math.PI / 180));
					renderer.camPosChangedNotify();
				}
				if (as.mouseLook)
				{
					Point mousePosScreen = MouseInfo.getPointerInfo().getLocation();
					
					int dxi = mousePosCenter.x - mousePosScreen.x;
					int dyi = mousePosCenter.y - mousePosScreen.y;
					if (as.invertY) dyi = -dyi;
					
					if (!((dxi == 0) && (dyi == 0)))
					{
						float dx = dxi;
						float dy = dyi;
						as.cameraPosition.camRotX = as.cameraPosition.camRotX + ((dy / 20) * as.sensitivity);
						as.cameraPosition.camRotY = as.cameraPosition.camRotY + ((dx / 20) * as.sensitivity);
						if (as.cameraPosition.camRotX > 360) as.cameraPosition.camRotX = as.cameraPosition.camRotX - 360;
						else if (as.cameraPosition.camRotX < 0) as.cameraPosition.camRotX = 360 + as.cameraPosition.camRotX;
						if (as.cameraPosition.camRotY > 360) as.cameraPosition.camRotY = as.cameraPosition.camRotY - 360;
						else if (as.cameraPosition.camRotY < 0) as.cameraPosition.camRotY = 360 + as.cameraPosition.camRotY;
						
						try
						{
							Robot r = new Robot();
							r.mouseMove(mousePosCenter.x, mousePosCenter.y);
						}
						catch (AWTException e) 
						{
						}
						
						//Select the tile we are looking at
						//Create view vector, by rotating default (0,0,1) the same way as we do in opengl
						Vector3 viewVector = Vector3Math.rotateZ(Vector3Math.rotateY(Vector3Math.rotateX(new Vector3(0, 0, 1), as.cameraPosition.camRotX * Math.PI / 180), as.cameraPosition.camRotY * Math.PI / 180), as.cameraPosition.camRotZ * Math.PI / 180);
						Vector3 intersectPoint = Vector3Math.planeIntersectPoint(new Vector3(0, sme.maxHeight / 2, 0), new Vector3(0, 1, 0), new Vector3(as.cameraPosition.camX, as.cameraPosition.camY, as.cameraPosition.camZ), viewVector);
						if (intersectPoint != null)
						{
							//System.out.println("X:" + intersectPoint.x() + "  Y: " + intersectPoint.y() +"  Z: " + intersectPoint.z());
							sme.mes.setBrushPos((int)(intersectPoint.x() / as.quadSize) - (sme.mes.activeBrush.width / 2) - 1,
												(int)(intersectPoint.z() / as.quadSize) - (sme.mes.activeBrush.height / 2) - 1);
						}
						
						renderer.camViewChangedNotify();
					}
				}
				else
				{
					if (!mousePos.equals(mousePosOld))
					{
						//Modify basic viewVector
						float modRotX = -((((float)mousePos.y / as.displayHeight) - 0.5f) * (as.fov / (as.displayWidth / as.displayHeight)));
						float modRotY = -((((float)mousePos.x / as.displayWidth) - 0.5f) * as.fov);
						Vector3 mouseVector = new Vector3(0, 0, 1);
						mouseVector = Vector3Math.rotateY(Vector3Math.rotateX(mouseVector, modRotX * Math.PI / 180), modRotY * Math.PI / 180);
						//Create view vector, by rotating default (0,0,1) the same way as we do in opengl
						Vector3 viewVector = Vector3Math.rotateZ(Vector3Math.rotateY(Vector3Math.rotateX(mouseVector, as.cameraPosition.camRotX * Math.PI / 180), as.cameraPosition.camRotY * Math.PI / 180), as.cameraPosition.camRotZ * Math.PI / 180);
						Vector3 intersectPoint = Vector3Math.planeIntersectPoint(new Vector3(0, sme.maxHeight / 2, 0), new Vector3(0, 1, 0), new Vector3(as.cameraPosition.camX, as.cameraPosition.camY, as.cameraPosition.camZ), viewVector);
						if (intersectPoint != null)
						{
							//System.out.println("X:" + intersectPoint.x() + "  Y: " + intersectPoint.y() +"  Z: " + intersectPoint.z());
							sme.mes.setBrushPos((int)(intersectPoint.x() / as.quadSize) - (sme.mes.activeBrush.width / 2) - 1,
												(int)(intersectPoint.z() / as.quadSize) - (sme.mes.activeBrush.height / 2) - 1);
						}
						mousePosOld = mousePos;
						
						renderer.camViewChangedNotify();
					}
				}
				//Handle edit
				if (holdableKeys[HoldableKeys.MOUSE_1.ordinal()])
				{
					/*
					 * TODO this whole code should be moved somewhere else here only:
					 * sme.applyBrush(invert); here: sme.applyBrush(false);
					 */
					switch (sme.mes.getBrushMode())
					{
						case Height:
						{
							switch (sme.mes.getHeightBrush().mode)
							{
								case Raise: sme.modifyHeight(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getHeightBrush(), false); break;
								case Smooth: sme.smoothHeight(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getHeightBrush(), sme.mes.getHeightBrush().getStrength()); break;
								case Set: sme.setHeight(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getHeightBrush().width, sme.mes.getHeightBrush().height, sme.mes.getHeightBrush().getStrength() / sme.maxHeight); break;
								case Erode: 
								{
									sme.erodeMapWet(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getHeightBrush().width, sme.mes.getHeightBrush().height);
									holdableKeys[HoldableKeys.MOUSE_1.ordinal()] = false;
									break;
								}
								case Randomize: sme.randomizeHeight(sme.mes.brushPos.x(),  sme.mes.brushPos.y(), sme.mes.getHeightBrush().width, sme.mes.getHeightBrush().height, sme.mes.getHeightBrush().getStrength());
							}
							renderer.invalidateBlocksByBrush(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getHeightBrush(), true, false, false);
							break;
						}
						case Texture:
						{
							switch (sme.mes.getTextureBrush().mode)
							{
								case Set: sme.setColorToTexture(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getTextureBrush()); break;
								case Add: sme.addColorToTexture(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getTextureBrush()); break;
								case Blend: sme.blendColorToTexture(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getTextureBrush()); break;
								case Stamp:
								{
									sme.stampColorToTexture(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getTextureBrush());
									holdableKeys[HoldableKeys.MOUSE_1.ordinal()] = false;
									break;
								}
								case TexGen: sme.genColorsByHeight(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getTextureBrush()); break;
							}
							renderer.invalidateBlocksByBrush(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getTextureBrush(), false, true, false);
							break;
						}
						case Metal:
						{
							switch (sme.mes.getMetalBrush().mode)
							{
								case Set: sme.setToMetalmap(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getMetalBrush()); break;
								case Add: sme.addToMetalmap(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getMetalBrush(), false); break;												
							}
							renderer.invalidateBlocksByBrush(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getMetalBrush(), false, true, false);
							break;
						}
						case Type:
						{
							sme.setToTypemap(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getTypeBrush(), false);
							renderer.invalidateBlocksByBrush(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getTypeBrush(), false, true, false);
							break;
						}
						case Vegetation:
						{
							sme.setToVegetationmap(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getVegetationBrush(), false);
							renderer.invalidateBlocksByBrush(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getVegetationBrush(), false, true, false);
							break;
						}
						case Feature:
						{
							switch (sme.mes.getFeatureBrush().mode)
							{
								case Add:
								{
									sme.addToFeaturemap(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getFeatureBrush());
									renderer.invalidateBlocksByBrush(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getFeatureBrush(), false, false, true);
									holdableKeys[HoldableKeys.MOUSE_1.ordinal()] = false;
									break;
								}
								case Rotate: sme.rotateFeaturemap(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getFeatureBrush(), false); break;
								case RotateSameRandom: sme.rotateSameRandom(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getFeatureBrush()); break;
							}
							break;
						}
						case Prefab:
						{
							switch (sme.mes.getPrefabBrush().mode)
							{
								case Set:
								{
									sme.setPrefab(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getPrefabBrush());
									holdableKeys[HoldableKeys.MOUSE_1.ordinal()] = false;
									renderer.invalidateBlocksByBrush(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getPrefabBrush(), true, true, false);
									break;
								}
								case Add:
								{
									sme.addPrefab(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getPrefabBrush());
									holdableKeys[HoldableKeys.MOUSE_1.ordinal()] = false;
									renderer.invalidateBlocksByBrush(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getPrefabBrush(), true, true, false);
									break;
								}
							}
							break;
						}
					}
				}
				if (holdableKeys[HoldableKeys.MOUSE_3.ordinal()])
				{
					switch (sme.mes.getBrushMode())
					{
						case Height:
						{
							switch (sme.mes.getHeightBrush().mode)
							{
								case Raise: sme.modifyHeight(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getHeightBrush(), true); break;
								case Smooth: sme.smoothHeight(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getHeightBrush(), sme.mes.getHeightBrush().getStrength()); break;
								case Set: sme.setHeight(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getHeightBrush().width, sme.mes.getHeightBrush().height, sme.mes.getHeightBrush().getStrength() / sme.maxHeight); break;
								case Erode:
								{
									sme.erodeMapDryWet(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getHeightBrush().width, sme.mes.getHeightBrush().height, false);
									holdableKeys[HoldableKeys.MOUSE_3.ordinal()] = false;
									break;
								}
								case Randomize: sme.randomizeHeight(sme.mes.brushPos.x(),  sme.mes.brushPos.y(), sme.mes.getHeightBrush().width, sme.mes.getHeightBrush().height, sme.mes.getHeightBrush().getStrength() / 4);
							}
							renderer.invalidateBlocksByBrush(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getHeightBrush(), true, false, false);
							break;
						}
						case Texture:
						{
							switch (sme.mes.getTextureBrush().mode)
							{
								case Set: sme.setColorToTexture(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getTextureBrush()); break;
								case Add: sme.addColorToTexture(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getTextureBrush()); break;
								case Blend: sme.blendColorToTexture(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getTextureBrush()); break;
								case Stamp:
								{
									sme.stampColorToTexture(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getTextureBrush());
									holdableKeys[HoldableKeys.MOUSE_3.ordinal()] = false;
									break;
								}
								case TexGen: sme.genColorsByHeight(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getTextureBrush()); break;
							}
							renderer.invalidateBlocksByBrush(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getTextureBrush(), false, true, false);
							break;
						}
						case Metal:
						{
							switch (sme.mes.getMetalBrush().mode)
							{
								case Set: sme.setToMetalmap(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getMetalBrush()); break;
								case Add: sme.addToMetalmap(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getMetalBrush(), true); break;												
							}
							renderer.invalidateBlocksByBrush(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getMetalBrush(), false, true, false);
							break;
						}
						case Type:
						{
							sme.setToTypemap(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getTypeBrush(), true);
							renderer.invalidateBlocksByBrush(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getTypeBrush(), false, true, false);
							break;
						}
						case Vegetation:
						{
							sme.setToVegetationmap(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getVegetationBrush(), true);
							renderer.invalidateBlocksByBrush(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getVegetationBrush(), false, true, false);
							break;
						}
						case Feature:
						{
							switch (sme.mes.getFeatureBrush().mode)
							{
								case Add:
								{
									sme.removeFromFeaturemap(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getFeatureBrush());
									renderer.invalidateBlocksByBrush(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getFeatureBrush(), false, false, true);
									break;
								}
								case Rotate: sme.rotateFeaturemap(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getFeatureBrush(), true); break;
								case RotateSameRandom: sme.rotateSameRandom(sme.mes.brushPos.x(), sme.mes.brushPos.y(), sme.mes.getFeatureBrush()); break;
							}
							break;
						}
					}
				}
							
				//Set title
				as.fps = fps.getFps();
				shell.setText("FPS: " + as.fps + "  Tris rendered: " + as.trisRendered + " viewing: " + as.mapMode.toString());
				
				//Process render independent Message Queue (does not depend on GL)
				//Do work which relies on this thread as executor. (everything which modifies some state)
				{
					Command cmd = messageQueue.poll();
					while (cmd != null)
					{
						cmd.execute(null);
						cmd = messageQueue.poll();
					}
				}
				
				ticksSinceLastRender++;
			}
			//Ensure we render at least once every 100 world ticks.
			if ((!makeTickThisLoop) || (ticksSinceLastRender > 100))
			{
				if (as.renderFullspeed || (ticksSinceLastRender > 0))
				{
					//Activate glContext 
					glCanvas.setCurrent();
					glContext.makeCurrent();
					
					//Render
					renderer.display(glContext.getGL());
					glCanvas.swapBuffers();
					
					//Animate some GUI components
					if (as.animateGUI && (ticksSinceLastRender > 0))
					{
						setupDialog.animate();
					}
					
					//Process GLMessage Queue withing glContext
					//Do work which relies on this thread as executor. (everything which modifies some state or uses the GL is)
					{
						Command cmd = glMessageQueue.poll();
						while (cmd != null)
						{
							cmd.execute(null);
							cmd = glMessageQueue.poll();
						}
					}
					
					glContext.release();

					ticksSinceLastRender = 0;
					fps.tick();
				}
				if (!as.renderFullspeed)
				{
					/* Here we calculate how long we need to sleep.
					 * Only do the sleep, if the sleeptime is higher than our minimum.
					 * This minimum usually is around 1-16ms
					 * Note the minimum should not be higher than 1 / worldFPS,
					 * which is currently 33. 1/33 = 30ms
					 * 
					 * This means if minSleepTime is higher than 30ms, the System is never going to sleep.
					 * This also means if (worldtick + rendering) takes more than 30-minSleep = 14ms, we never sleep.
					 * 14ms equals a theoretical fps of 1/0.014 = 72 !!!
					 * Which is relatively high.
					 * So only on highspec systems there will be some cpu preservation.
					 * (I guess most slower systems will be cpu bound anyway)
					 */
					sleepTime = animationTimeStep - (System.nanoTime() - lastAnimation);
					if (sleepTime > minSleepTime)
						LockSupport.parkNanos(sleepTime);
				}	
			}
			
			//Process GUI
			display.readAndDispatch();
		}
		//MAIN LOOP END
		
		//Save settings
		keyMap.saveToFile(new File("config/keys.cfg"));
		as.saveToFile(new File("config/settings.cfg"));
		
		//Dispose Display
		display.dispose();
		
		//Close Dialog too
		setupDialog.dispose();
	}
	
}
