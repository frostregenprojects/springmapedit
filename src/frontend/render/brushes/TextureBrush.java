/*
 * SpringMapEdit -- A 3D map editor for the Spring engine
 *
 * Copyright (C) 2008-2009  Heiko Schmitt <heikos23@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
/**
 * TextureBrush.java 
 * Created on 10.10.2008
 * by Heiko Schmitt
 */
package frontend.render.brushes;

/**
 * @author Heiko Schmitt
 *
 */
public class TextureBrush extends Brush
{
	//Brush Manager
	private BrushDataManager<BrushPattern> brushPatternManager;
	private BrushDataManager<BrushTexture> brushTextureManager;
	
	//Pattern
	public BrushPattern pattern;
	public BrushTexture texture;
	
	//Current Mode
	public TextureMode mode;
	
	//Brush settings
	public float[] strength;
	
	//Brush in repeat mode, or stamp mode?
	boolean[] moduloMode;
	
	public enum TextureMode
	{
		Set,
		Add,
		Blend,
		Stamp,
		TexGen
	}
	
	public TextureBrush(BrushDataManager<BrushPattern> brushPatternManager, BrushDataManager<BrushTexture> brushTextureManager, int patternID, int textureID, int width, int height, int textureWidth, int textureHeight)
	{
		this.brushPatternManager = brushPatternManager;
		this.brushTextureManager = brushTextureManager;
		
		mode = TextureMode.Blend;
		int count = TextureMode.values().length;
		
		moduloMode = new boolean[count];
		moduloMode[TextureMode.Set.ordinal()] = false;
		moduloMode[TextureMode.Add.ordinal()] = false;
		moduloMode[TextureMode.Blend.ordinal()] = false;
		moduloMode[TextureMode.Stamp.ordinal()] = false;
		moduloMode[TextureMode.TexGen.ordinal()] = false;
		
		strength = new float[count];
		strength[TextureMode.Set.ordinal()] = 1f;
		strength[TextureMode.Add.ordinal()] = 0.1f;
		strength[TextureMode.Blend.ordinal()] = 1f;
		strength[TextureMode.Stamp.ordinal()] = 1f;
		strength[TextureMode.TexGen.ordinal()] = 1f;
		
		//Set up pattern and texture
		this.pattern = brushPatternManager.getScaledBrushData(patternID, width, height, true);
		this.texture = brushTextureManager.getScaledBrushData(textureID, textureWidth, textureHeight, true);
		
		//Set size
		this.width = pattern.width;
		this.height = pattern.width;
	}

	public float getStrength()
	{
		return strength[mode.ordinal()];
	}
	
	public int getStrengthInt()
	{
		int result = 0;
		switch (mode)
		{
			case Set:    result = (int)(strength[mode.ordinal()] * 1000); break;
			case Add:    result = (int)(strength[mode.ordinal()] * 1000); break;
			case Blend:  result = (int)(strength[mode.ordinal()] * 1000); break;
			case Stamp:  result = (int)(strength[mode.ordinal()] * 1000); break;
			case TexGen: result = (int)(strength[mode.ordinal()] * 1000); break;
		}
		return result;
	}
	
	public void setStrengthInt(int strength)
	{
		switch (mode)
		{
			case Set:    this.strength[mode.ordinal()] = strength / 1000f; break;
			case Add:    this.strength[mode.ordinal()] = strength / 1000f; break;
			case Blend:  this.strength[mode.ordinal()] = strength / 1000f; break;
			case Stamp:  this.strength[mode.ordinal()] = strength / 1000f; break;
			case TexGen: this.strength[mode.ordinal()] = strength / 1000f; break;
		}
	}
	
	public boolean getModuloMode()
	{
		return moduloMode[mode.ordinal()];
	}
	
	@Override
	public void setSize(int width, int height)
	{
		super.setSize(width, height);
		setPattern(pattern.patternID);
		//setTexture(texture.textureID);
	}
	
	public int getPatternID()
	{
		return pattern.patternID;
	}
	
	public void setPattern(int patternID)
	{
		this.pattern = brushPatternManager.getScaledBrushData(patternID, width, height, true);
		this.width = pattern.width;
		this.height = pattern.height;
	}
	
	public int getTextureID()
	{
		return texture.textureID;
	}
	
	public void setTexture(int textureID)
	{
		this.texture = brushTextureManager.getScaledBrushData(textureID, -1, -1, true);
	}
	
	public void setTextureToColor(int r, int g, int b)
	{
		this.texture = new BrushTexture(r, g, b);
	}
	
	@Override
	public void rotate(boolean counterClockWise)
	{
		//Rotating the pattern makes not much sense here
		texture.rotate(counterClockWise);
	}
	
	@Override
	public void mirror(boolean horizontal)
	{
		texture.mirror(horizontal);
	}
}
