/*
 * SpringMapEdit -- A 3D map editor for the Spring engine
 *
 * Copyright (C) 2008-2009  Heiko Schmitt <heikos23@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
/**
 * PrefabBrush.java 
 * Created on 28.09.2009
 * by Heiko Schmitt
 */
package frontend.render.brushes;

import backend.FastMath;
import backend.SpringMapEdit;
import backend.math.Vector2Int;

/**
 * @author Heiko Schmitt
 *
 */
public class PrefabBrush extends Brush
{
	//Manager for PrefabData
	private PrefabManager prefabManager;
	
	//Prefab Data
	public PrefabData prefab;
	public BrushPattern heightmap;
	public BrushTexture texturemap;
	
	Vector2Int origDimension;
	double ratio;
	double dWidth;
	
	//Current Mode
	public PrefabMode mode;
	
	//Brush settings
	public float[] strength;
	
	public enum PrefabMode
	{
		Set,
		Add
	}

	/**
	 * @param width
	 * @param height
	 */
	public PrefabBrush(PrefabManager prefabManager, int prefabID, int width, int height)
	{
		this.prefabManager = prefabManager;
		
		mode = PrefabMode.Set;
		int count = PrefabMode.values().length;
		
		strength = new float[count];
		strength[PrefabMode.Set.ordinal()] = 1f;
		strength[PrefabMode.Add.ordinal()] = 1f;
		
		//Set up pattern and texture
		this.prefab = prefabManager.getPrefabData(prefabID);
		this.heightmap = prefabManager.getScaledPrefabHeightmap(prefabID, width, height, true);
		//Set size
		if (heightmap != null)
		{
			this.width = heightmap.width;
			this.height = heightmap.height;
			this.texturemap = prefabManager.getScaledPrefabTexturemap(prefabID, (heightmap.width - 1) * SpringMapEdit.heightmapSizeTextureFactor, (heightmap.height - 1) * SpringMapEdit.heightmapSizeTextureFactor, false);
		}
		else
		{
			this.texturemap = prefabManager.getScaledPrefabTexturemap(prefabID, (width - 1) * SpringMapEdit.heightmapSizeTextureFactor, (height - 1) * SpringMapEdit.heightmapSizeTextureFactor, true);
			if (texturemap != null)
			{
				this.width = (texturemap.width / 8) + 1;
				this.height = (texturemap.height / 8) + 1;
			}
			else
			{
				this.width = 1;
				this.height = 1;
			}
		}
		dWidth = this.width;
		updateRatio();
	}
	
	private void updateRatio()
	{
		if (prefab != null)
		{
			origDimension = prefabManager.getPrefabOriginalDimension(prefab.prefabID);
			if (origDimension != null)
			{
				ratio = dWidth / (double)origDimension.vector[0];
			}
		}
	}
	
	public float getStrength()
	{
		return strength[mode.ordinal()];
	}
	
	public int getStrengthInt()
	{
		int result = 0;
		switch (mode)
		{
			case Set:    result = (int)strength[mode.ordinal()]; break;
			case Add:    result = (int)strength[mode.ordinal()]; break;
		}
		return result;
	}

	public void setStrengthInt(int strength)
	{
		switch (mode)
		{
			case Set:    this.strength[mode.ordinal()] = strength; break;
			case Add:    this.strength[mode.ordinal()] = strength; break;
		}
	}
	
	@Override
	public void setSize(int width, int height)
	{	
		super.setSize(width, height);
		setPrefab(prefab.prefabID);
		updateRatio();
	}
	
	public void setPrefab(int prefabID)
	{
		if (width < 0)
		{
			Vector2Int newDimension = prefabManager.getPrefabOriginalDimension(prefabID);
			if (newDimension != null)
			{
				dWidth = newDimension.vector[0] * ratio;
				width = FastMath.round(dWidth);
			}
		}
		
		prefab = prefabManager.getPrefabData(prefabID);
		heightmap = prefabManager.getScaledPrefabHeightmap(prefabID, width, height, true);
		if (heightmap != null)
		{
			width = heightmap.width;
			height = heightmap.height;
			texturemap = prefabManager.getScaledPrefabTexturemap(prefabID, (heightmap.width - 1) * SpringMapEdit.heightmapSizeTextureFactor, (heightmap.height - 1) * SpringMapEdit.heightmapSizeTextureFactor, false);
		}
		else
		{
			this.texturemap = prefabManager.getScaledPrefabTexturemap(prefabID, (width - 1) * SpringMapEdit.heightmapSizeTextureFactor, (height - 1) * SpringMapEdit.heightmapSizeTextureFactor, true);
			if (texturemap != null)
			{
				width = (texturemap.width / 8) + 1;
				height = (texturemap.height / 8) + 1;
			}
			else
			{
				width = 1;
				height = 1;
			}
		}
		if (FastMath.round(dWidth) != width)
			dWidth = width;
	}
	
	public void setPattern(int patternID)
	{
		setPrefab(patternID);
	}
	
	public int getPrefabID()
	{
		return prefab.prefabID;
	}
	

	public void rotate(boolean counterClockWise)
	{
		if (heightmap != null)
		{
			heightmap.rotate(counterClockWise);
			width = heightmap.width;
			height = heightmap.height;
		}
		if (texturemap != null)
		{
			texturemap.rotate(counterClockWise);
			if (heightmap == null)
			{
				width = (texturemap.width / 8) + 1;
				height = (texturemap.height / 8) + 1;
			}
		}
	}
	
	public void mirror(boolean horizontal)
	{
		if (heightmap != null)
		{
			heightmap.mirror(horizontal);
		}
		if (texturemap != null)
		{
			texturemap.mirror(horizontal);
		}
	}
}
