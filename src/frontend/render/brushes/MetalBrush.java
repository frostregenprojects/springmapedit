/*
 * SpringMapEdit -- A 3D map editor for the Spring engine
 *
 * Copyright (C) 2008-2009  Heiko Schmitt <heikos23@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
/**
 * MetalBrush.java 
 * Created on 10.10.2008
 * by Heiko Schmitt
 */
package frontend.render.brushes;

/**
 * @author Heiko Schmitt
 *
 */
public class MetalBrush extends Brush
{
	//Brush Manager
	private BrushDataManager<BrushPattern> brushPatternManager;
	
	//Pattern
	public BrushPattern pattern;
	
	//Current Brush mode
	public MetalMode mode;
	
	//Brush settings
	public float[] strength;
	
	//Brush in repeat mode, or stamp mode?
	boolean[] moduloMode;
	
	//Modes
	public enum MetalMode
	{
		Add,
		Set
	}
	
	public MetalBrush(BrushDataManager<BrushPattern> brushPatternManager, int patternID, int width, int height)
	{
		this.brushPatternManager = brushPatternManager;
		
		mode = MetalMode.Add;
		int count = MetalMode.values().length;
		
		moduloMode = new boolean[count];
		moduloMode[MetalMode.Add.ordinal()] = false;
		moduloMode[MetalMode.Set.ordinal()] = false;
		                         
		strength = new float[count];
		strength[MetalMode.Add.ordinal()] = 10f;
		strength[MetalMode.Set.ordinal()] = 0.5f;
		
		//Set up pattern and texture
		this.pattern = brushPatternManager.getScaledBrushData(patternID, width, height, true);
		
		//Set size
		this.width = pattern.width;
		this.height = pattern.height;
	}

	public float getStrength()
	{
		return strength[mode.ordinal()];
	}
	
	public int getStrengthInt()
	{
		int result = 0;
		switch (mode)
		{
			case Add: result = (int)strength[mode.ordinal()]; break;
			case Set: result = (int)(strength[mode.ordinal()] * 1000); break;
		}
		return result;
	}
	
	public void setStrengthInt(int strength)
	{
		switch (mode)
		{
			case Add: this.strength[mode.ordinal()] = strength; break;
			case Set: this.strength[mode.ordinal()] = strength / 1000f; break;
			
		}
	}
	
	public boolean getModuloMode()
	{
		return moduloMode[mode.ordinal()];
	}
	
	@Override
	public void setSize(int width, int height)
	{
		super.setSize(width, height);
		setPattern(pattern.patternID);
	}
	
	public int getPatternID()
	{
		return pattern.patternID;
	}
	
	public void setPattern(int patternID)
	{
		this.pattern = brushPatternManager.getScaledBrushData(patternID, width, height, true);
		this.width = pattern.width;
		this.height = pattern.height;
	}
	
	@Override
	public void rotate(boolean counterClockWise)
	{
		pattern.rotate(counterClockWise);
		width = pattern.width;
		height = pattern.height;
	}
	
	@Override
	public void mirror(boolean horizontal)
	{
		pattern.mirror(horizontal);
	}
}
