/*
 * SpringMapEdit -- A 3D map editor for the Spring engine
 *
 * Copyright (C) 2008-2009  Heiko Schmitt <heikos23@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
/**
 * Brush.java 
 * Created on 05.07.2008
 * by Heiko Schmitt
 */
package frontend.render.brushes;

/**
 * @author Heiko Schmitt
 *
 */
public class Brush
{
	public int width;
	public int height;
	
	public Brush()
	{
		width = -1;
		height = -1;
	}
	
	public Brush(int width, int height)
	{
		this.width = width;
		this.height = height;
	}
	
	public void setSize(int width, int height)
	{
		this.width = width;
		this.height = height;
	}
	
	/**
	 * This method should return the Strength value stretched to range 1-1000
	 */
	public int getStrengthInt()
	{
		return 0;
	}
	
	/**
	 * This method expects the Strength value stretched to range 1-1000
	 * @param strength
	 */
	public void setStrengthInt(int strength)
	{
		//does nothing
	}
	
	/**
	 * Whether this brush operates on vertices, or on tiles
	 * @return
	 */
	public boolean isVertexOriented()
	{
		return false;
	}
	
	/**
	 * This method returns the actual strength value
	 * @return
	 */
	public float getStrength()
	{
		return 0;
	}
	
	public int getPatternID()
	{
		return 0;
	}
	
	public void setPattern(int patternID)
	{
		//does nothing
	}
	
	public int getTextureID()
	{
		return 0;
	}
	
	public void setTexture(int textureID)
	{
		//does nothing
	}
	
	public void setTextureToColor(int r, int g, int b)
	{
		//does nothing
	}
	
	public void mirror(boolean horizontal)
	{
		//does nothing
	}
	
	public void rotate(boolean counterClockWise)
	{
		//does nothing
	}
}
