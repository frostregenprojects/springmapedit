/*
 * SpringMapEdit -- A 3D map editor for the Spring engine
 *
 * Copyright (C) 2008-2009  Heiko Schmitt <heikos23@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
/**
 * FeatureBrush.java 
 * Created on 10.10.2008
 * by Heiko Schmitt
 */
package frontend.render.brushes;

/**
 * @author Heiko Schmitt
 *
 */
public class FeatureBrush extends Brush
{
	//Current mode
	public FeatureMode mode;
	
	//Brush settings
	public int featureID;
	public float rotateStrength;
	
	//Brush in repeat mode, or stamp mode?
	boolean[] moduloMode;
	
	public enum FeatureMode
	{
		Add,
		Rotate,
		RotateSameRandom
	}
	
	public FeatureBrush(int patternID, int width, int height)
	{
		mode = FeatureMode.Add;
		featureID = 0;
		rotateStrength = 0.5f;
		
		moduloMode = new boolean[FeatureMode.values().length];
		moduloMode[FeatureMode.Add.ordinal()] = false;
		moduloMode[FeatureMode.Rotate.ordinal()] = false;
		moduloMode[FeatureMode.RotateSameRandom.ordinal()] = false;
		
		//Set size
		this.width = width;
		this.height = height;
	}
	
	public int getStrengthInt()
	{
		int result = 0;
		switch (mode)
		{
			case Add: result = featureID; break;
			case Rotate: result = (int)(rotateStrength * 10); break;
			case RotateSameRandom: result = 0; break;
		}
		return result;
	}
	
	public void setStrengthInt(int strength)
	{
		switch (mode)
		{
			case Add: featureID = strength; break;
			case Rotate: rotateStrength = strength / 10f; break;
			case RotateSameRandom: break;
		}
	}
	
	public float getStrength()
	{
		float result = 0;
		switch (mode)
		{
			case Add: result = featureID; break;
			case Rotate: result = rotateStrength; break;
			case RotateSameRandom: result = 0; break;
		}
		return result;
	}
	
	public int getPatternID()
	{
		return 0;
	}
	
	public void setPattern(int patternID)
	{
		//Do nothing
	}
}
