/*
 * SpringMapEdit -- A 3D map editor for the Spring engine
 *
 * Copyright (C) 2008-2009  Heiko Schmitt <heikos23@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
/**
 * SpringMapEdit.java 
 * Created on 03.07.2008
 * by Heiko Schmitt
 */
package backend;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

import frontend.render.AppSettings;
import frontend.render.MapRenderer;
import frontend.render.brushes.BrushTexture;
import frontend.render.brushes.FeatureBrush;
import frontend.render.brushes.HeightBrush;
import frontend.render.brushes.MetalBrush;
import frontend.render.brushes.PrefabBrush;
import frontend.render.brushes.TextureBrush;
import frontend.render.brushes.TypeBrush;
import frontend.render.brushes.VegetationBrush;
import frontend.render.features.FeatureMapContainer;

import backend.FileHandler.FileFormat;
import backend.image.Bitmap;
import backend.image.ImageGrayscaleFloat;
import backend.image.ImageLoader;
import backend.image.ImageSaver;
import backend.io.ByteInputStream;
import backend.io.DataInputStream;
import backend.io.DataOutputStream;
import backend.math.Point;
import backend.sm2.SM2File;
import backend.sm3.SM3Layer;

/**
 * @author Heiko Schmitt
 *
 */
public class SpringMapEdit
{
	/**
	 * This is the factor we need to multiply the spring mapsize with,<BR>
	 * to get the actual heightmap-size. (do not forget to add 1 pixel to heightmap)
	 */
	public static final int springMapsizeHeightmapFactor = 64;
	
	/**
	 * This is the factor we need to multiply the spring mapsize with,<BR>
	 * to get the actual texture-size.
	 */
	public static final int springMapsizeTexturemapFactor = springMapsizeHeightmapFactor * 8;
	
	/**
	 * This is the factor we need to multiply the spring mapsize with,<BR>
	 * to get the actual metalmap-size.
	 */
	public static final int springMapsizeMetalmapFactor = springMapsizeHeightmapFactor / 2;
	
	/**
	 * This is the factor we need to multiply the spring mapsize with,<BR>
	 * to get the actual typemap-size.
	 */
	public static final int springMapsizeTypemapFactor = springMapsizeHeightmapFactor / 2;
	
	/**
	 * This is the factor we need to multiply the spring mapsize with,<BR>
	 * to get the actual vegetationmap-size.
	 */
	public static final int springMapsizeVegetationmapFactor = springMapsizeHeightmapFactor / 4;
	
	/**
	 * This is the factor we need to multiply the spring mapsize with,<BR>
	 * to get the actual slopemap-size.
	 */
	public static final int springMapsizeSlopemapFactor = springMapsizeHeightmapFactor / 2;
	
	/**
	 * This is the factor we need to multiply the mapsize with,<BR>
	 * to get the actual texture-size.
	 */
	public static final int heightmapSizeTextureFactor = springMapsizeTexturemapFactor / springMapsizeHeightmapFactor;
	
	/**
	 * This is the factor we need to divide the mapsize by,<BR>
	 * to get the actual metalmap-size.
	 */
	public static final int heightmapSizeMetalmapDivisor = springMapsizeHeightmapFactor / springMapsizeMetalmapFactor;
	
	/**
	 * This is the factor we need to divide the mapsize by,<BR>
	 * to get the actual typemap-size.
	 */
	public static final int heightmapSizeTypemapDivisor = springMapsizeHeightmapFactor / springMapsizeTypemapFactor;
	
	/**
	 * This is the factor we need to divide the mapsize by,<BR>
	 * to get the actual typemap-size.
	 */
	public static final int heightmapSizeVegetationmapDivisor = springMapsizeHeightmapFactor / springMapsizeVegetationmapFactor;
	
	/**
	 * This is the virtual (openGL) size of one Tile.<BR>
	 * This is needed for feature Placement.
	 */
	public static int tileSize = 2;
	
	/**
	 * Width in spring units
	 */
	public int width;
	/**
	 * Height in spring units
	 */
	public int height; 
	
	public float[][] heightMap; 
	public int heightMapWidth;
	public int heightMapHeight;
	
	/**
	 * NOTE: This array is indexed [y][x] for faster transfer to VRAM
	 * SM2 only
	 */
	public byte[][] textureMap;
	public int textureMapWidth;
	public int textureMapHeight;
	
	public SM3Layer[] sm3Layers;
		
	public final int featureBlockSizeInTiles = 32;
	public int featureMapWidthInBlocks;
	public int featureMapHeightInBlocks;
	public int featureBlockCount;
	public ArrayList<FeatureMapContainer>[] featureList;
	
	public byte[][] metalMap; 
	public int metalMapWidth;
	public int metalMapHeight;
	
	public byte[][] typeMapColorTable;
	public byte[][] typeMap; 
	public int typeMapWidth;
	public int typeMapHeight;
	
	public byte[][] vegetationMap; 
	public int vegetationMapWidth;
	public int vegetationMapHeight;
	
	public int[] slopes;
	public byte[][] slopeMapColorTable;
	public byte[][] slopeMap; 
	public int slopeMapWidth;
	public int slopeMapHeight;
	
	public float waterHeight;
	public int maxHeight;
	
	//Editing stuff
	public MapEditSettings mes;
	public AppSettings as;
	
	//Quicksave/Quickload stuff
	private QuickSave quicksave;
		
	/**
	 * Create a new map.<BR>
	 * Sizes are in Spring map units!
	 */
	public SpringMapEdit(int width, int height, MapEditSettings mes, AppSettings as)
	{
		this.as = as;
		SpringMapEdit.tileSize = as.quadSize;
		
		if (mes == null) 
			this.mes = new MapEditSettings(as);
		else
			this.mes = mes;
		
		init();
		
		this.width = width;
		this.height = height;
		
		allocateMap();
		
		genStartupHeightmap();
		whiteOutTextureMap();
		blankFeatureMap();
	}
	
	/**
	 * 
	 */
	public SpringMapEdit(File heightmapFile, MapEditSettings mes)
	{
		if (mes == null) 
			this.mes = new MapEditSettings(as);
		else
			this.mes = mes;
		
		init();
		loadDataIntoHeightmap(heightmapFile);
	}
		
	private void init()
	{
		//Setup some defaults
		this.maxHeight = tileSize * 80; //20 = TTD
		this.waterHeight = maxHeight / 4f;
		
		//Quicksave storage
		quicksave = new QuickSave();
		
		//Slope Colors
		int slopeCount = 4;
		slopes = new int[slopeCount];
		slopeMapColorTable = new byte[slopeCount][3];
		slopeMapColorTable[0][0] = (byte) 0;
		slopeMapColorTable[0][1] = (byte) 0;
		slopeMapColorTable[0][2] = (byte) 0;
		slopes[0] = (int)FastMath.round(0.08 * 255);
		
		slopeMapColorTable[1][0] = (byte) 0;
		slopeMapColorTable[1][1] = (byte) 255;
		slopeMapColorTable[1][2] = (byte) 0;
		slopes[1] = (int)FastMath.round(0.22 * 255);
		
		slopeMapColorTable[2][0] = (byte) 0;
		slopeMapColorTable[2][1] = (byte) 0;
		slopeMapColorTable[2][2] = (byte) 255;
		slopes[2] = (int)FastMath.round(0.56 * 255);
		
		slopeMapColorTable[3][0] = (byte) 255;
		slopeMapColorTable[3][1] = (byte) 0;
		slopeMapColorTable[3][2] = (byte) 0;
		slopes[3] = (int)255;
		
		//Type Colors
		int colors = 256;
		typeMapColorTable = new byte[colors][3];
		for (int i = 0; i < colors; i++)
		{
			typeMapColorTable[i][0] = (byte) i;
			typeMapColorTable[i][1] = (byte) 0;
			typeMapColorTable[i][2] = (byte) 0;
		}
		//Some distinguishable colors for first types
		typeMapColorTable[1][0] = (byte) 255;
		typeMapColorTable[1][1] = (byte) 255;
		typeMapColorTable[1][2] = (byte) 0;
		
		typeMapColorTable[2][0] = (byte) 0;
		typeMapColorTable[2][1] = (byte) 255;
		typeMapColorTable[2][2] = (byte) 0;
		
		typeMapColorTable[3][0] = (byte) 0;
		typeMapColorTable[3][1] = (byte) 255;
		typeMapColorTable[3][2] = (byte) 255;
		
		typeMapColorTable[4][0] = (byte) 0;
		typeMapColorTable[4][1] = (byte) 0;
		typeMapColorTable[4][2] = (byte) 255;
		
		typeMapColorTable[5][0] = (byte) 255;
		typeMapColorTable[5][1] = (byte) 0;
		typeMapColorTable[5][2] = (byte) 255;
		
		typeMapColorTable[6][0] = (byte) 255;
		typeMapColorTable[6][1] = (byte) 158;
		typeMapColorTable[6][2] = (byte) 0;
		
		typeMapColorTable[7][0] = (byte) 158;
		typeMapColorTable[7][1] = (byte) 255;
		typeMapColorTable[7][2] = (byte) 0;
		
		typeMapColorTable[8][0] = (byte) 0;
		typeMapColorTable[8][1] = (byte) 255;
		typeMapColorTable[8][2] = (byte) 158;
		
		typeMapColorTable[9][0] = (byte) 158;
		typeMapColorTable[9][1] = (byte) 0;
		typeMapColorTable[9][2] = (byte) 255;
	}
	
	public void saveHeightMap(File heightmapFile, FileFormat fileFormat)
	{
		if (fileFormat == FileFormat.PNG16Bit)
		{
			ImageGrayscaleFloat image = new ImageGrayscaleFloat(heightMap);
			ImageSaver.saveImageGrayscaleFloat(heightmapFile, image, fileFormat, true);
		}
		else
		{
			Bitmap bitmap = new Bitmap(fileFormat);
			bitmap.saveDataFromHeightmap(heightmapFile, heightMap, heightMapWidth, heightMapHeight);
		}
	}
	
	public void saveTextureMap(File texturemapFile)
	{
		Bitmap bitmap = new Bitmap(FileFormat.Bitmap24Bit);
		bitmap.saveDataFromTexturemap(texturemapFile, textureMap, textureMapWidth, textureMapHeight);
	}
	
	public void saveMetalMap(File metalmapFile, boolean use24Bit)
	{
		Bitmap bitmap = new Bitmap(use24Bit ? FileFormat.Bitmap24Bit : FileFormat.Bitmap8Bit);
		bitmap.saveDataFromMetalmap(metalmapFile, metalMap, metalMapWidth, metalMapHeight);
	}
	
	public void saveTypeMap(File typemapFile, boolean use24Bit)
	{
		Bitmap bitmap = new Bitmap(use24Bit ? FileFormat.Bitmap24Bit : FileFormat.Bitmap8Bit);
		bitmap.saveDataFromTypemap(typemapFile, typeMap, typeMapWidth, typeMapHeight);
	}
	
	public void saveVegetationMap(File vegetationmapFile)
	{
		Bitmap bitmap = new Bitmap(FileFormat.Bitmap8Bit);
		bitmap.saveDataFromVegetationmap(vegetationmapFile, vegetationMap, vegetationMapWidth, vegetationMapHeight);
	}
	
	public void saveFeatureMap(File featuremapFile, MapRenderer renderer)
	{
		SM2File sm2fh = new SM2File(this, renderer);
		sm2fh.saveSMFFeaturesToFile(featuremapFile);
	}
	
	public void saveSlopeMap(File slopemapFile)
	{
		Bitmap bitmap = new Bitmap(FileFormat.Bitmap8Bit);
		bitmap.saveDataFromSlopemap(slopemapFile, slopeMap, slopeMapWidth, slopeMapHeight);
	}
	
	public void saveSM2Map(File filename, MapRenderer renderer)
	{		
		SM2File sm2fh = new SM2File(this, renderer);
		if (!as.minimapFilename.equals("")) sm2fh.setExternalMinimap(as.minimapFilename);
		if (!as.texturemapFilename.equals("")) sm2fh.setExternalTexturemap(as.texturemapFilename);
		sm2fh.save(filename);
	}
	
	public void saveAllMaps(File filename, MapRenderer renderer)
	{		
		String name = filename.getName();
		File dir = filename.getParentFile();
		
		saveHeightMap(new File(dir, name + "_Height"), FileFormat.PNG16Bit);
		saveMetalMap(new File(dir, name + "_Metal"), true);
		saveFeatureMap(new File(dir, name + "_Feature"), renderer);
		saveTypeMap(new File(dir, name + "_Type"), false);
		saveVegetationMap(new File(dir, name + "_Vegetation"));
		saveTextureMap(new File(dir, name + "_Texture"));
	}
	
	public void loadSM2Map(File filename, MapRenderer renderer)
	{		
		SM2File sm2fh = new SM2File(this, renderer);
		sm2fh.load(filename);
	}
	
	public void loadAllMaps(File filename, MapRenderer renderer)
	{		
		String name = filename.getName();
		File dir = filename.getParentFile();
		
		//remove extension
		if (name.endsWith("_Height.png")) name = name.substring(0, name.length() - 11);
		if (name.endsWith("_Metal.bmp")) name = name.substring(0, name.length() - 10);
		if (name.endsWith("_Feature.fmf")) name = name.substring(0, name.length() - 12);
		if (name.endsWith("_Type.bmp")) name = name.substring(0, name.length() - 9);
		if (name.endsWith("_Vegetation.bmp")) name = name.substring(0, name.length() - 15);
		if (name.endsWith("_Texture.bmp")) name = name.substring(0, name.length() - 12);
		
		loadDataIntoHeightmap(new File(dir, name + "_Height.png"));
		loadDataIntoMetalmap(new File(dir, name + "_Metal.bmp"));
		loadDataIntoFeaturemap(new File(dir, name + "_Feature.fmf"), renderer);
		loadDataIntoTypemap(new File(dir, name + "_Type.bmp"));
		loadDataIntoVegetationmap(new File(dir, name + "_Vegetation.bmp"));
		loadDataIntoTexturemap(new File(dir, name + "_Texture.bmp"));
	}
	
	private void resetMap()
	{
		width = 0;
		height = 0;
		
		heightMap = null; 
		heightMapWidth = 0;
		heightMapHeight = 0;
		
		metalMap = null;
		metalMapWidth = 0;
		metalMapHeight = 0;
		
		typeMap = null;
		typeMapWidth = 0;
		typeMapHeight = 0;
		
		vegetationMap = null;
		vegetationMapWidth = 0;
		vegetationMapHeight = 0;
		
		slopeMap = null;
		slopeMapWidth = 0;
		slopeMapHeight = 0;
		
		textureMap = null;
		textureMapWidth = 0;
		textureMapHeight = 0;
		
		for (int i = 0; i < featureBlockCount; i++)
			featureList[i] = null;
		
		featureList = null;
		featureBlockCount = 0;
		featureMapWidthInBlocks = 0;
		featureMapHeightInBlocks = 0;
		
		quicksave.free();
		
		//Free Resources
		System.gc();
		System.gc();
	}
	
	@SuppressWarnings("unchecked")
	public void allocateMap()
	{
		heightMapWidth = (width * springMapsizeHeightmapFactor) + 1;
		heightMapHeight = (height * springMapsizeHeightmapFactor) + 1;
		heightMap = new float[heightMapWidth][heightMapHeight];
	
		metalMapWidth = width * springMapsizeMetalmapFactor;
		metalMapHeight = height * springMapsizeMetalmapFactor;
		metalMap = new byte[metalMapWidth][metalMapHeight];
		
		typeMapWidth = width * springMapsizeTypemapFactor;
		typeMapHeight = height * springMapsizeTypemapFactor;
		typeMap = new byte[this.typeMapWidth][this.typeMapHeight];
		
		vegetationMapWidth = width * springMapsizeVegetationmapFactor;
		vegetationMapHeight = height * springMapsizeVegetationmapFactor;
		vegetationMap = new byte[this.vegetationMapWidth][this.vegetationMapHeight];
		
		slopeMapWidth = width * springMapsizeSlopemapFactor;
		slopeMapHeight = height * springMapsizeSlopemapFactor;
		slopeMap = new byte[this.slopeMapWidth][this.slopeMapHeight];
		
		textureMapWidth = width * springMapsizeTexturemapFactor;
		textureMapHeight = height * springMapsizeTexturemapFactor;
		if ( !(as.batchMode && (!as.texturemapFilename.equals(""))) ) //Do not allocate textureMap, if in batchmode and DDS is given
			textureMap = new byte[textureMapHeight][textureMapWidth * 3];
		
		featureMapWidthInBlocks = width * springMapsizeHeightmapFactor / featureBlockSizeInTiles;
		featureMapHeightInBlocks = height * springMapsizeHeightmapFactor / featureBlockSizeInTiles;
		featureBlockCount = featureMapWidthInBlocks * featureMapHeightInBlocks;
		featureList = (ArrayList<FeatureMapContainer>[])new ArrayList[featureBlockCount];
		for (int i = 0; i < featureBlockCount; i++)
			this.featureList[i] = new ArrayList<FeatureMapContainer>();
	}
	
	public void newMap(int newWidth, int newHeight, boolean genRandom)
	{
		try
		{
			boolean needsReset = ((width != newWidth) || (height != newHeight));
			
			//Free Resources if needed
			if (needsReset) resetMap();
			
			//Change width/height
			width = newWidth;
			height = newHeight;
						
			//Allocate new data arrays if needed
			if (needsReset) allocateMap();
			
			//genRandom, if wanted
			if (genRandom) genStartupHeightmap();
		}
		catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * NOTE: This skips RAW checking. Do not use for loading RAW's!
	 * @param heightmapFile
	 */
	public void loadDataIntoHeightmap(File heightmapFile)
	{
		if (FileHandler.isHandledByBitmap(heightmapFile))
		{
			Bitmap bitmap = new Bitmap(heightmapFile);
			loadDataIntoHeightmap(bitmap);
		}
		else
		{
			ImageGrayscaleFloat image = ImageLoader.loadImageGrayscaleFloat(heightmapFile, true);
			loadDataIntoHeightmap(image);
		}
	}
	
	public void loadDataIntoHeightmap(Bitmap bitmap)
	{
		try
		{
			boolean needsReset = ((this.heightMapWidth != bitmap.width) || (this.heightMapHeight != bitmap.height));
			
			//Check new sizes
			if (((bitmap.width - 1) % springMapsizeHeightmapFactor) != 0) throw new IllegalArgumentException("Heightmap Width-1 must be dividable by " + springMapsizeHeightmapFactor);
			if (((bitmap.height - 1) % springMapsizeHeightmapFactor) != 0) throw new IllegalArgumentException("Heightmap Height-1 must be dividable by " + springMapsizeHeightmapFactor);
			
			//Free Resources if needed
			if (needsReset) resetMap();
			
			//Change width/height
			width = (bitmap.width - 1) / springMapsizeHeightmapFactor;
			height = (bitmap.height - 1) / springMapsizeHeightmapFactor;
			
			//Allocate new data arrays if needed
			if (needsReset) allocateMap();
			
			//Read Bitmap data into Heightmap
			bitmap.loadDataIntoHeightmap(this.heightMap);
		}
		catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		}
	}
	
	public void loadDataIntoHeightmap(ImageGrayscaleFloat image)
	{
		try
		{
			boolean needsReset = ((this.heightMapWidth != image.width) || (this.heightMapHeight != image.height));
			
			//Check new sizes
			if (((image.width - 1) % springMapsizeHeightmapFactor) != 0) throw new IllegalArgumentException("Heightmap Width-1 must be dividable by " + springMapsizeHeightmapFactor);
			if (((image.height - 1) % springMapsizeHeightmapFactor) != 0) throw new IllegalArgumentException("Heightmap Height-1 must be dividable by " + springMapsizeHeightmapFactor);
			
			//Free Resources if needed
			if (needsReset) resetMap();
			
			//Change width/height
			width = (image.width - 1) / springMapsizeHeightmapFactor;
			height = (image.height - 1) / springMapsizeHeightmapFactor;
			
			//Allocate new data arrays if needed
			if (needsReset) allocateMap();
			
			//Copy image data into Heightmap
			float[][] data = image.data;
			for (int x = 0; x < image.width; x++)
				System.arraycopy(data[x], 0, heightMap[x], 0, image.height);
		}
		catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		}
	}
	
	public void loadDataIntoTexturemap(File texturemapFile)
	{
		try
		{
			Bitmap bitmap = new Bitmap(texturemapFile);
			if ((bitmap.width == width * springMapsizeTexturemapFactor) && (bitmap.height == height * springMapsizeTexturemapFactor))
			{
				bitmap.loadDataIntoTexturemap(textureMap);
			}
			else
				throw new IllegalArgumentException("TextureMapsize must be " + heightmapSizeTextureFactor + " * (heightmapsize - 1)");
		}
		catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		}
	}
	
	public void loadDataIntoMetalmap(File metalmapFile)
	{
		try
		{
			Bitmap bitmap = new Bitmap(metalmapFile);
			if ((bitmap.width == width * springMapsizeMetalmapFactor) && (bitmap.height == height * springMapsizeMetalmapFactor))
			{
				bitmap.loadDataIntoMetalmap(metalMap);
			}
			else
				throw new IllegalArgumentException("MetalMapsize must be (heightmapsize - 1) / 2");
		}
		catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		}
	}
	
	public void loadDataIntoTypemap(File typemapFile)
	{
		try
		{
			Bitmap bitmap = new Bitmap(typemapFile);
			if ((bitmap.width == width * springMapsizeTypemapFactor) && (bitmap.height == height * springMapsizeTypemapFactor))
			{
				bitmap.loadDataIntoTypemap(typeMap);
			}
			else
				throw new IllegalArgumentException("TypeMapsize must be (heightmapsize - 1) / 2");
		}
		catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		}
	}
	
	public void loadDataIntoVegetationmap(File vegetationFile)
	{
		try
		{
			Bitmap bitmap = new Bitmap(vegetationFile);
			if ((bitmap.width == width * springMapsizeVegetationmapFactor) && (bitmap.height == height * springMapsizeVegetationmapFactor))
			{
				bitmap.loadDataIntoVegetationmap(vegetationMap);
			}
			else
				throw new IllegalArgumentException("VegetationMapsize must be (heightmapsize - 1) / 4");
		}
		catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		}
	}
	
	public void loadDataIntoFeaturemap(File featuremapFile, MapRenderer renderer)
	{
		//Remove all Features
		blankFeatureMap();
		
		//Load Features
		SM2File sm2f = new SM2File(this, renderer);
		sm2f.loadSMFFeaturesFromFile(featuremapFile);
	}

	public void modifyHeight(int px, int py, HeightBrush brush, boolean invert)
	{
		float amount = brush.getStrength();
		if (invert) amount = -amount;
		float[][] pattern = brush.pattern.getPattern();
		for (int y = py; y < py + brush.height; y++)
		{
			for (int x = px; x < px + brush.width; x++)
			{
				if ((x >= 0) && (x < heightMapWidth) && (y >= 0) && (y < heightMapHeight))
				{
					heightMap[x][y] = heightMap[x][y] + (amount * pattern[x - px][y - py]);
					if (heightMap[x][y] > 1) heightMap[x][y] = 1;
					else if (heightMap[x][y] < 0) heightMap[x][y] = 0;
				}
			}
		}
	}
	
	public void setHeight(int px, int py, int width, int height, float setHeight)
	{
		for (int y = py; y < py + height; y++)
		{
			for (int x = px; x < px + width; x++)
			{
				if ((x >= 0) && (x < heightMapWidth) && (y >= 0) && (y < heightMapHeight))
				{
					heightMap[x][y] = setHeight;
					if (heightMap[x][y] > 1) heightMap[x][y] = 1;
					else if (heightMap[x][y] < 0) heightMap[x][y] = 0;
				}
			}
		}
	}
	
	public void setHeightToMap(float setHeight)
	{
		setHeight(0, 0, heightMapWidth, heightMapHeight, setHeight);
	}
	
	public void smoothHeight(int px, int py, HeightBrush brush, float limit)
	{
		float[][] smoothedMap = new float[brush.width][brush.height];
		for (int y = py; y < py + brush.height; y++)
		{
			for (int x = px; x < px + brush.width; x++)
			{
				if ((x >= 0) && (x < heightMapWidth) && (y >= 0) && (y < heightMapHeight))
				{
					smoothedMap[x-px][y-py] = smooth9HeightmapBorderAware(limit, x, y);
				}
			}
		}
		for (int y = py; y < py + brush.height; y++)
		{
			for (int x = px; x < px + brush.width; x++)
			{
				if ((x >= 0) && (x < heightMapWidth) && (y >= 0) && (y < heightMapHeight))
				{
					heightMap[x][y] = smoothedMap[x-px][y-py];
				}
			}
		}
	}
	
	public void smoothMap(float limit)
	{
		long start = System.nanoTime();
		int x, y;
		float[][] smoothedMap = new float[heightMapWidth][heightMapHeight];
		
		//Center (No Border checks required -> faster)
		for (y = 1; y < heightMapHeight-1; y++)
		{
			for (x = 1; x < heightMapWidth-1; x++)
			{
				smoothedMap[x][y] = smooth9(limit, heightMap[x][y], heightMap[x-1][y-1], heightMap[x][y-1], heightMap[x+1][y-1], heightMap[x-1][y], heightMap[x+1][y], heightMap[x-1][y+1], heightMap[x][y+1], heightMap[x+1][y+1]);
			}
		}
		
		//Left Border
		for (y = 1; y < heightMapHeight-1; y++)
			smoothedMap[0][y] = smooth9HeightmapBorderAware(limit, 0, y);
		
		//Right Border
		for (y = 1; y < heightMapHeight-1; y++)
			smoothedMap[heightMapWidth - 1][y] = smooth9HeightmapBorderAware(limit, heightMapWidth - 1, y);
		
		//Upper Border
		for (x = 0; x < heightMapWidth; x++)
			smoothedMap[x][0] = smooth9HeightmapBorderAware(limit, x, 0);
		
		//Lower Border
		for (x = 0; x < heightMapWidth; x++)
			smoothedMap[x][heightMapHeight - 1] = smooth9HeightmapBorderAware(limit, x, heightMapHeight - 1);
		
		//Copy Back
		for (x = 0; x < heightMapWidth; x++)
			System.arraycopy(smoothedMap[x], 0, heightMap[x], 0, heightMapHeight);
		
		System.out.println("Done smoothing heightMap ( " + ((System.nanoTime() - start) / 1000000) + " ms )");
	}
	
	public void setColorToTexture(int px, int py, TextureBrush brush)
	{
		float amount = brush.getStrength();
		byte[][] textureR = brush.texture.getTextureR();
		byte[][] textureG = brush.texture.getTextureG();
		byte[][] textureB = brush.texture.getTextureB();
		px = (px * heightmapSizeTextureFactor);
		py = (py * heightmapSizeTextureFactor);
		int r, g, b;
		for (int y = py; y < py + (brush.height * heightmapSizeTextureFactor); y++)
		{
			for (int x = px; x < px + (brush.width * heightmapSizeTextureFactor); x++)
			{
				if ((x >= 0) && (x < textureMapWidth) && (y >= 0) && (y < textureMapHeight))
				{
					r = FastMath.round((amount * (textureR[x % brush.texture.width][y % brush.texture.height] & 0xFF)));
					g = FastMath.round((amount * (textureG[x % brush.texture.width][y % brush.texture.height] & 0xFF)));
					b = FastMath.round((amount * (textureB[x % brush.texture.width][y % brush.texture.height] & 0xFF)));
							
					textureMap[y][(x*3) + 0] = (byte)Math.min(Math.max(r, 0), 255);
					textureMap[y][(x*3) + 1] = (byte)Math.min(Math.max(g, 0), 255);
					textureMap[y][(x*3) + 2] = (byte)Math.min(Math.max(b, 0), 255);
				}
			}
		}
	}
	
	public void addColorToTexture(int px, int py, TextureBrush brush)
	{
		float amount = brush.getStrength();
		float[][] pattern = brush.pattern.getPattern();
		byte[][] textureR = brush.texture.getTextureR();
		byte[][] textureG = brush.texture.getTextureG();
		byte[][] textureB = brush.texture.getTextureB();
		px = (px * heightmapSizeTextureFactor);
		py = (py * heightmapSizeTextureFactor);
		int r, g, b;
		for (int y = py; y < py + (brush.height * heightmapSizeTextureFactor); y++)
		{
			for (int x = px; x < px + (brush.width * heightmapSizeTextureFactor); x++)
			{
				if ((x >= 0) && (x < textureMapWidth) && (y >= 0) && (y < textureMapHeight))
				{
					r = FastMath.round((textureMap[y][(x*3) + 0] & 0xFF) + (amount * pattern[((x-px) / heightmapSizeTextureFactor)][((y-py) / heightmapSizeTextureFactor)] * (textureR[x % brush.texture.width][y % brush.texture.height] & 0xFF)));
					g = FastMath.round((textureMap[y][(x*3) + 1] & 0xFF) + (amount * pattern[((x-px) / heightmapSizeTextureFactor)][((y-py) / heightmapSizeTextureFactor)] * (textureG[x % brush.texture.width][y % brush.texture.height] & 0xFF)));
					b = FastMath.round((textureMap[y][(x*3) + 2] & 0xFF) + (amount * pattern[((x-px) / heightmapSizeTextureFactor)][((y-py) / heightmapSizeTextureFactor)] * (textureB[x % brush.texture.width][y % brush.texture.height] & 0xFF)));
					
					textureMap[y][(x*3) + 0] = (byte)Math.min(Math.max(r, 0), 255);
					textureMap[y][(x*3) + 1] = (byte)Math.min(Math.max(g, 0), 255);
					textureMap[y][(x*3) + 2] = (byte)Math.min(Math.max(b, 0), 255);
				}
			}
		}
	}
	
	public void blendColorToTexture(int px, int py, TextureBrush brush)
	{
		float amount = brush.getStrength();
		float[][] pattern = brush.pattern.getPattern();
		int patternWidth = brush.pattern.width;
		int patternHeight = brush.pattern.height;
		byte[][] textureR = brush.texture.getTextureR();
		byte[][] textureG = brush.texture.getTextureG();
		byte[][] textureB = brush.texture.getTextureB();
		px = (px * heightmapSizeTextureFactor);
		py = (py * heightmapSizeTextureFactor);
		int r, g, b;
		int patternX, patternY;
		float blendFactorX, blendFactorY, leftVal, rightVal;
		float newTexAmountOrigin, newTexAmountLower, newTexAmountRight, newTexAmountLowerRight, newTexAmount, invNewTexAmount;
		for (int y = py; y < py + (brush.height * heightmapSizeTextureFactor); y++)
		{
			for (int x = px; x < px + (brush.width * heightmapSizeTextureFactor); x++)
			{
				if ((x >= 0) && (x < textureMapWidth) && (y >= 0) && (y < textureMapHeight))
				{
					patternX = ((x-px) / heightmapSizeTextureFactor);
					patternY = ((y-py) / heightmapSizeTextureFactor);
					newTexAmountOrigin = amount * pattern[patternX][patternY];
					newTexAmountRight = newTexAmountOrigin;
					newTexAmountLower = newTexAmountOrigin;
					newTexAmountLowerRight = newTexAmountOrigin;
					if ((patternX + 1) < patternWidth)  newTexAmountRight = amount * pattern[patternX + 1][patternY];
					if ((patternY + 1) < patternHeight) newTexAmountLower = amount * pattern[patternX][patternY + 1];
					if (((patternX + 1) < patternWidth) && ((patternY + 1) < patternHeight)) newTexAmountLowerRight = amount * pattern[patternX + 1][patternY + 1];
					blendFactorX = ((x-px) % heightmapSizeTextureFactor) / (float)heightmapSizeTextureFactor;
					blendFactorY = ((y-py) % heightmapSizeTextureFactor) / (float)heightmapSizeTextureFactor;
					leftVal = (newTexAmountOrigin * (1 - blendFactorY)) + (newTexAmountLower * blendFactorY);
					rightVal = (newTexAmountRight * (1 - blendFactorY)) + (newTexAmountLowerRight * blendFactorY);
					newTexAmount = (leftVal * (1 - blendFactorX)) + (rightVal * blendFactorX);

					invNewTexAmount = 1 - newTexAmount;
					r = FastMath.round((invNewTexAmount * (textureMap[y][(x*3) + 0] & 0xFF)) + (newTexAmount * (textureR[x % brush.texture.width][y % brush.texture.height] & 0xFF)));
					g = FastMath.round((invNewTexAmount * (textureMap[y][(x*3) + 1] & 0xFF)) + (newTexAmount * (textureG[x % brush.texture.width][y % brush.texture.height] & 0xFF)));
					b = FastMath.round((invNewTexAmount * (textureMap[y][(x*3) + 2] & 0xFF)) + (newTexAmount * (textureB[x % brush.texture.width][y % brush.texture.height] & 0xFF)));
					
					textureMap[y][(x*3) + 0] = (byte)Math.min(Math.max(r, 0), 255);
					textureMap[y][(x*3) + 1] = (byte)Math.min(Math.max(g, 0), 255);
					textureMap[y][(x*3) + 2] = (byte)Math.min(Math.max(b, 0), 255);
				}
			}
		}
	}
	
	public void stampColorToTexture(int px, int py, TextureBrush brush)
	{
		float amount = brush.getStrength();
		byte[][] textureR = brush.texture.getTextureR();
		byte[][] textureG = brush.texture.getTextureG();
		byte[][] textureB = brush.texture.getTextureB();
		byte[][] textureA = brush.texture.getTextureA();
		px = (px * heightmapSizeTextureFactor);
		py = (py * heightmapSizeTextureFactor);
		int r, g, b;
		for (int y = py; y < py + (brush.height * heightmapSizeTextureFactor); y++)
		{
			for (int x = px; x < px + (brush.width * heightmapSizeTextureFactor); x++)
			{
				if ((x >= 0) && (x < textureMapWidth) && (y >= 0) && (y < textureMapHeight))
				{
					amount = (textureA[(x-px) % brush.texture.width][(y-py) % brush.texture.height] & 0xFF) / (float)0xFF;
					r = FastMath.round(((1 - amount) * (textureMap[y][(x*3) + 0] & 0xFF)) + (amount * (textureR[(x-px) % brush.texture.width][(y-py) % brush.texture.height] & 0xFF)));
					g = FastMath.round(((1 - amount) * (textureMap[y][(x*3) + 1] & 0xFF)) + (amount * (textureG[(x-px) % brush.texture.width][(y-py) % brush.texture.height] & 0xFF)));
					b = FastMath.round(((1 - amount) * (textureMap[y][(x*3) + 2] & 0xFF)) + (amount * (textureB[(x-px) % brush.texture.width][(y-py) % brush.texture.height] & 0xFF)));
							
					textureMap[y][(x*3) + 0] = (byte)Math.min(Math.max(r, 0), 255);
					textureMap[y][(x*3) + 1] = (byte)Math.min(Math.max(g, 0), 255);
					textureMap[y][(x*3) + 2] = (byte)Math.min(Math.max(b, 0), 255);
				}
			}
		}
	}
	
	public void addToMetalmap(int px, int py, MetalBrush brush, boolean invert)
	{
		float amount = brush.getStrength();
		if (invert) amount = -amount;
		float[][] pattern = brush.pattern.getPattern();
		px = (px / heightmapSizeMetalmapDivisor);
		py = (py / heightmapSizeMetalmapDivisor);
		int val;
		for (int y = py; y < py + (brush.height / heightmapSizeMetalmapDivisor); y++)
		{
			for (int x = px; x < px + (brush.width / heightmapSizeMetalmapDivisor); x++)
			{
				if ((x >= 0) && (x < metalMapWidth) && (y >= 0) && (y < metalMapHeight))
				{
					val = FastMath.round((metalMap[x][y] & 0xFF) + (amount * pattern[((x-px) * heightmapSizeMetalmapDivisor)][((y-py) * heightmapSizeMetalmapDivisor)]));
					
					metalMap[x][y] = (byte)Math.min(Math.max(val, 0), 255);
				}
			}
		}
	}
	
	public void setToMetalmap(int px, int py, MetalBrush brush)
	{
		float amount = brush.getStrength();
		px = (px / heightmapSizeMetalmapDivisor);
		py = (py / heightmapSizeMetalmapDivisor);
		for (int y = py; y < py + (brush.height / heightmapSizeMetalmapDivisor); y++)
		{
			for (int x = px; x < px + (brush.width / heightmapSizeMetalmapDivisor); x++)
			{
				if ((x >= 0) && (x < metalMapWidth) && (y >= 0) && (y < metalMapHeight))
				{
					metalMap[x][y] = (byte)Math.min(Math.max(amount * 255, 0), 255);
				}
			}
		}
	}
	
	public void setToTypemap(int px, int py, TypeBrush brush, boolean invert)
	{
		int type = brush.typeID;
		if (invert) type = 0;
		px = (px / heightmapSizeTypemapDivisor);
		py = (py / heightmapSizeTypemapDivisor);
		for (int y = py; y < py + (brush.height / heightmapSizeTypemapDivisor); y++)
		{
			for (int x = px; x < px + (brush.width / heightmapSizeTypemapDivisor); x++)
			{
				if ((x >= 0) && (x < typeMapWidth) && (y >= 0) && (y < typeMapHeight))
				{
					typeMap[x][y] = (byte)Math.min(Math.max(type, 0), 255);
				}
			}
		}
	}
	
	public void setToVegetationmap(int px, int py, VegetationBrush brush, boolean invert)
	{
		int type = brush.typeID;
		if (invert) type = 0;
		px = (px / heightmapSizeVegetationmapDivisor);
		py = (py / heightmapSizeVegetationmapDivisor);
		for (int y = py; y < py + (brush.height / heightmapSizeVegetationmapDivisor); y++)
		{
			for (int x = px; x < px + (brush.width / heightmapSizeVegetationmapDivisor); x++)
			{
				if ((x >= 0) && (x < vegetationMapWidth) && (y >= 0) && (y < vegetationMapHeight))
				{
					vegetationMap[x][y] = (byte)Math.min(Math.max(type, 0), 1);
				}
			}
		}
	}
	
	public void setPrefab(int px, int py, PrefabBrush brush)
	{
		//Set Heightmap to given Data:
		if (brush.heightmap != null)
		{
			int level = brush.getStrengthInt() - 1;
			float amount = brush.getStrength() / maxHeight;
			if (mes.brushHeightAlign != 0)
				amount = (level * mes.brushHeightAlign) - (level * brush.prefab.levelOverlap * brush.prefab.heightZ);
			float scale = brush.prefab.heightZ;
			float[][] pattern = brush.heightmap.getPattern();
			for (int y = py; y < py + brush.height; y++)
			{
				for (int x = px; x < px + brush.width; x++)
				{
					if ((x >= 0) && (x < heightMapWidth) && (y >= 0) && (y < heightMapHeight))
					{
						heightMap[x][y] = amount + (scale * pattern[x - px][y - py]);
						if (heightMap[x][y] > 1) heightMap[x][y] = 1;
						else if (heightMap[x][y] < 0) heightMap[x][y] = 0;
					}
				}
			}
		}
		
		//Set Colormap to given Data
		if (brush.texturemap != null)
		{
			byte[][] textureR = brush.texturemap.getTextureR();
			byte[][] textureG = brush.texturemap.getTextureG();
			byte[][] textureB = brush.texturemap.getTextureB();
			px = (px * heightmapSizeTextureFactor);
			py = (py * heightmapSizeTextureFactor);
			int r, g, b;
			for (int y = py; y < py + ((brush.height - 1) * heightmapSizeTextureFactor); y++)
			{
				for (int x = px; x < px + ((brush.width - 1) * heightmapSizeTextureFactor); x++)
				{
					if ((x >= 0) && (x < textureMapWidth) && (y >= 0) && (y < textureMapHeight))
					{
						r = textureR[(x-px) % brush.texturemap.width][(y-py) % brush.texturemap.height] & 0xFF;
						g = textureG[(x-px) % brush.texturemap.width][(y-py) % brush.texturemap.height] & 0xFF;
						b = textureB[(x-px) % brush.texturemap.width][(y-py) % brush.texturemap.height] & 0xFF;
								
						textureMap[y][(x*3) + 0] = (byte)Math.min(Math.max(r, 0), 255);
						textureMap[y][(x*3) + 1] = (byte)Math.min(Math.max(g, 0), 255);
						textureMap[y][(x*3) + 2] = (byte)Math.min(Math.max(b, 0), 255);
					}
				}
			}
		}
	}
	
	public void addPrefab(int px, int py, PrefabBrush brush)
	{
		//Set Heightmap to given Data:
		if (brush.heightmap != null)
		{
			float scale = brush.prefab.heightZ;
			float[][] pattern = brush.heightmap.getPattern();
			for (int y = py; y < py + brush.height; y++)
			{
				for (int x = px; x < px + brush.width; x++)
				{
					if ((x >= 0) && (x < heightMapWidth) && (y >= 0) && (y < heightMapHeight))
					{
						heightMap[x][y] = heightMap[x][y] + (scale * pattern[x - px][y - py]);
						if (heightMap[x][y] > 1) heightMap[x][y] = 1;
						else if (heightMap[x][y] < 0) heightMap[x][y] = 0;
					}
				}
			}
		}
		
		//Set Colormap to given Data
		if (brush.texturemap != null)
		{
			byte[][] textureR = brush.texturemap.getTextureR();
			byte[][] textureG = brush.texturemap.getTextureG();
			byte[][] textureB = brush.texturemap.getTextureB();
			px = (px * heightmapSizeTextureFactor);
			py = (py * heightmapSizeTextureFactor);
			int r, g, b;
			for (int y = py; y < py + ((brush.height - 1) * heightmapSizeTextureFactor); y++)
			{
				for (int x = px; x < px + ((brush.width - 1) * heightmapSizeTextureFactor); x++)
				{
					if ((x >= 0) && (x < textureMapWidth) && (y >= 0) && (y < textureMapHeight))
					{
						r = textureR[(x-px) % brush.texturemap.width][(y-py) % brush.texturemap.height] & 0xFF;
						g = textureG[(x-px) % brush.texturemap.width][(y-py) % brush.texturemap.height] & 0xFF;
						b = textureB[(x-px) % brush.texturemap.width][(y-py) % brush.texturemap.height] & 0xFF;
								
						textureMap[y][(x*3) + 0] = (byte)Math.min(Math.max(r, 0), 255);
						textureMap[y][(x*3) + 1] = (byte)Math.min(Math.max(g, 0), 255);
						textureMap[y][(x*3) + 2] = (byte)Math.min(Math.max(b, 0), 255);
					}
				}
			}
		}
	}
	
	public void addToFeaturemap(int px, int py, FeatureBrush brush)
	{
		int type = brush.featureID;
		int x = px + (brush.width / 2);
		int y = py + (brush.height / 2);
		if ((x >= 0) && (x < heightMapWidth) && (y >= 0) && (y < heightMapHeight))
			addFeature(x * tileSize, y * tileSize, 0, type);
	}
	
	public void removeFromFeaturemap(int px, int py, FeatureBrush brush)
	{
		int x = px + (brush.width / 2);
		int y = py + (brush.height / 2);
		if ((x >= 0) && (x < heightMapWidth) && (y >= 0) && (y < heightMapHeight))
			removeAllFeaturesInRange(x * tileSize, y * tileSize, ((brush.width / 2f) * tileSize));
	}
	
	public void rotateFeaturemap(int px, int py, FeatureBrush brush, boolean invert)
	{
		float amount = brush.rotateStrength;
		if (invert) amount = -amount;
		int x = px + (brush.width / 2);
		int y = py + (brush.height / 2);
		if ((x >= 0) && (x < heightMapWidth) && (y >= 0) && (y < heightMapHeight))
			rotateClosestFeature(x * tileSize, y * tileSize, ((brush.width / 2f) * tileSize), amount);
	}
	
	public void rotateSameRandom(int px, int py, FeatureBrush brush)
	{
		int xmiddle = px + (brush.width / 2);
		int ymiddle = py + (brush.height / 2);
		if ((xmiddle >= 0) && (xmiddle < heightMapWidth) && (ymiddle >= 0) && (ymiddle < heightMapHeight))
		{
			float x = xmiddle * tileSize;
			float z = ymiddle * tileSize;
			FeatureMapContainer f = findClosestFeature(x, z, ((brush.width / 2f) * tileSize));
			if (f != null)
			{
				int featureID = f.featureID;
				
				//Rotate all features with same id
				Random r = new Random();
				Iterator<FeatureMapContainer> it;
				for (int i = 0; i < featureBlockCount; i++)
				{
					it = featureList[i].iterator();
					while (it.hasNext())
					{
						f = it.next();
						if (f.featureID == featureID) f.rotY = r.nextFloat() * 360;
					}
				}
			}
		}
	}
	
	private float smooth9HeightmapBorderAware(float limit, int x, int y)
	{
		float value = heightMap[x][y];
		int pointsUsed = 1;
		
		if (y > 0) //Upper Border
		{
			if (limit < Math.abs(heightMap[x][y] - heightMap[x][y-1])) return heightMap[x][y];
			value += heightMap[x][y-1];
			pointsUsed++;
		}
		if (y < (heightMapHeight - 1)) //Lower Border
		{
			if (limit < Math.abs(heightMap[x][y] - heightMap[x][y+1])) return heightMap[x][y];
			value += heightMap[x][y+1];
			pointsUsed++;
		}
		if (x > 0) //Left Border
		{
			if (limit < Math.abs(heightMap[x][y] - heightMap[x-1][y])) return heightMap[x][y];
			value += heightMap[x-1][y];
			pointsUsed++;
			
			if (y > 0)
			{
				if (limit < Math.abs(heightMap[x][y] - heightMap[x-1][y-1])) return heightMap[x][y];
				value += heightMap[x-1][y-1];
				pointsUsed++;
			}
			if (y < (heightMapHeight - 1))
			{
				if (limit < Math.abs(heightMap[x][y] - heightMap[x-1][y+1])) return heightMap[x][y];
				value += heightMap[x-1][y+1];
				pointsUsed++;
			}
		}
		if (x < (heightMapWidth - 1)) //Right Border
		{
			if (limit < Math.abs(heightMap[x][y] - heightMap[x+1][y])) return heightMap[x][y];
			value += heightMap[x+1][y];
			pointsUsed++;
			
			if (y > 0)
			{
				if (limit < Math.abs(heightMap[x][y] - heightMap[x+1][y-1])) return heightMap[x][y];
				value += heightMap[x+1][y-1];
				pointsUsed++;
			}
			if (y < (heightMapHeight - 1))
			{
				if (limit < Math.abs(heightMap[x][y] - heightMap[x+1][y+1])) return heightMap[x][y];
				value += heightMap[x+1][y+1];
				pointsUsed++;
			}
		}
		
		return value / pointsUsed;
	}
	
	private float smooth9(float limit, float center, float v2, float v3, float v4, float v5, float v6, float v7, float v8, float v9)
	{
		if (limit < Math.abs(center - v2)) return center;
		if (limit < Math.abs(center - v3)) return center;
		if (limit < Math.abs(center - v4)) return center;
		if (limit < Math.abs(center - v5)) return center;
		if (limit < Math.abs(center - v6)) return center;
		if (limit < Math.abs(center - v7)) return center;
		if (limit < Math.abs(center - v8)) return center;
		if (limit < Math.abs(center - v9)) return center;
		return (center + v2 + v3 + v4 + v5 + v6 + v7 + v8 + v9) / 9;
	}
	
	private float getSmoothedSteepness(int x, int y, float fractX, float fractY)
	{
		float st1 = 0;
		float st2 = 0;
		float st3 = 0;
		float st4 = 0;
		int xMod = 0;
		int yMod = 0;
		
		
		float maxHeight = Math.max(Math.max(Math.max(heightMap[x + 0 + xMod][y + 0 + yMod], heightMap[x + 0 + xMod][y + 1 + yMod]), heightMap[x + 1 + xMod][y + 1 + yMod]), heightMap[x + 1 + xMod][y + 0 + yMod]);
		float minHeight = Math.min(Math.min(Math.min(heightMap[x + 0 + xMod][y + 0 + yMod], heightMap[x + 0 + xMod][y + 1 + yMod]), heightMap[x + 1 + xMod][y + 1 + yMod]), heightMap[x + 1 + xMod][y + 0 + yMod]);
		st1 = maxHeight - minHeight;
		
		if (y < (heightMapHeight - 2))
		{
			xMod = 0;
			yMod = 1;
			maxHeight = Math.max(Math.max(Math.max(heightMap[x + 0 + xMod][y + 0 + yMod], heightMap[x + 0 + xMod][y + 1 + yMod]), heightMap[x + 1 + xMod][y + 1 + yMod]), heightMap[x + 1 + xMod][y + 0 + yMod]);
			minHeight = Math.min(Math.min(Math.min(heightMap[x + 0 + xMod][y + 0 + yMod], heightMap[x + 0 + xMod][y + 1 + yMod]), heightMap[x + 1 + xMod][y + 1 + yMod]), heightMap[x + 1 + xMod][y + 0 + yMod]);
			st2 = maxHeight - minHeight;
		}
		
		if ((x < (heightMapWidth - 2)) && (y < (heightMapHeight - 2)))
		{
			xMod = 1;
			yMod = 1;
			maxHeight = Math.max(Math.max(Math.max(heightMap[x + 0 + xMod][y + 0 + yMod], heightMap[x + 0 + xMod][y + 1 + yMod]), heightMap[x + 1 + xMod][y + 1 + yMod]), heightMap[x + 1 + xMod][y + 0 + yMod]);
			minHeight = Math.min(Math.min(Math.min(heightMap[x + 0 + xMod][y + 0 + yMod], heightMap[x + 0 + xMod][y + 1 + yMod]), heightMap[x + 1 + xMod][y + 1 + yMod]), heightMap[x + 1 + xMod][y + 0 + yMod]);
			st3 = maxHeight - minHeight;
		}
		
		if (x < (heightMapWidth - 2))
		{
			xMod = 1;
			yMod = 0;
			maxHeight = Math.max(Math.max(Math.max(heightMap[x + 0 + xMod][y + 0 + yMod], heightMap[x + 0 + xMod][y + 1 + yMod]), heightMap[x + 1 + xMod][y + 1 + yMod]), heightMap[x + 1 + xMod][y + 0 + yMod]);
			minHeight = Math.min(Math.min(Math.min(heightMap[x + 0 + xMod][y + 0 + yMod], heightMap[x + 0 + xMod][y + 1 + yMod]), heightMap[x + 1 + xMod][y + 1 + yMod]), heightMap[x + 1 + xMod][y + 0 + yMod]);
			st4 = maxHeight - minHeight;
		}
		
		float steepYLeft =  ((1 - fractY) * st1) + (fractY * st2);
		float steepYRight = ((1 - fractY) * st4) + (fractY * st3);
		
		return ((1 - fractX) * steepYLeft) + (fractX * steepYRight);
	}
	
	/**
	 * Levels gives the number of brushes used.<BR>
	 * transitions should be:<BR>
	 * level0 end height,<BR>
	 * level1 start height, level1 end height,<BR>
	 * level2 start height, level2 end height,<BR>
	 * level3 start height, level3 end height,<BR>
	 * level4 start height<BR>
	 * <BR>
	 * So there should be 2 * (levels - 1) entries.<BR>
	 * If level end and next level start do not match,<BR>
	 * both levels will be blended within this margin.<BR>
	 * brushedFlat will be used if steepNess of triangle is smaller than given steepNess for Level<BR>
	 * otherwise brushesSteep will be used.<BR>
	 * level1 flatSteepness<BR>
	 * level1 steepSteepness<BR>
	 * level2 flatSteepnes<BR>
	 * level2 steepSteepness<BR>
	 * steepnesses between flatSteepness and steepSteepness will be blended<BR>
	 * @param brushesFlat
	 * @param brushesSteep
	 * @param transitions
	 * @param steepNess
	 * @param levels
	 */
	public void genColorsByHeight(int px, int py, TextureBrush brush)
	{
		TextureGeneratorSetup setup = mes.getTextureGeneratorSetup();
		if (setup == null)
		{
			return;
		}
		
		long start = System.nanoTime();
		final int oneTenthsOfHeight = Math.max(textureMapHeight / 10, 1);
		
		int levels = setup.levels;
		BrushTexture[] brushesFlat = setup.brushesFlat;
		BrushTexture[] brushesSteep = setup.brushesSteep;
		float[] heightTransitions = setup.heightTransitions;
		float[] steepTransitions = setup.steepTransitions;
		BrushTexture brush1Flat = brushesFlat[0];
		BrushTexture brush2Flat = brushesFlat[0];
		BrushTexture brush1Steep = brushesSteep[0];
		BrushTexture brush2Steep = brushesSteep[0];
		int xT, yT;
		int r1, g1, b1, r2, g2, b2;
		int r1s, g1s, b1s, r2s, g2s, b2s;
		float h1, h2, h3, h4, fractX, fractY, heightYLeft, heightYRight, height, steep;
		int transitionCount = (2 * (levels - 1));
		float blendFactorBrush2Flat = 0.5f;
		float blendFactorBrush2Steep = 0;
		
		px = (px * heightmapSizeTextureFactor);
		py = (py * heightmapSizeTextureFactor);
		int bWidth = heightMapWidth;
		int bHeight = heightMapHeight;
		if (brush != null)
		{
			bWidth = brush.width;
			bHeight = brush.height;
		}
		for (int y = py; y < py + (bHeight * heightmapSizeTextureFactor); y++)
		{
			fractY = (y % heightmapSizeTextureFactor) / (float)heightmapSizeTextureFactor;
		
			for (int x = px; x < px + (bWidth * heightmapSizeTextureFactor); x++)
			{
				if ((x >= 0) && (x < textureMapWidth) && (y >= 0) && (y < textureMapHeight))
				{
					fractX = (x % heightmapSizeTextureFactor) / (float)heightmapSizeTextureFactor;
					
					//calculate height of edge vertices
					xT = x / heightmapSizeTextureFactor;
					yT = y / heightmapSizeTextureFactor;
					
					h1 = heightMap[xT + 0][yT + 0];
					h2 = heightMap[xT + 0][yT + 1];
					h3 = heightMap[xT + 1][yT + 1];
					h4 = heightMap[xT + 1][yT + 0];
								
					//Steepness
					steep = getSmoothedSteepness(xT, yT, fractX, fractY);
					
					//calculate height of actual texel
					heightYLeft =  ((1 - fractY) * h1) + (fractY * h2);
					heightYRight = ((1 - fractY) * h4) + (fractY * h3);
					height = ((1 - fractX) * heightYLeft) + (fractX * heightYRight);
					
					//calculate which brushes to use
					if (height <= heightTransitions[0])
					{
						brush1Flat = brushesFlat[0];
						brush2Flat = brushesFlat[0];
						brush1Steep = brushesSteep[0];
						brush2Steep = brushesSteep[0];
						if (steep < steepTransitions[0])
						{
							blendFactorBrush2Steep = 0;
						} 
						else if (steep > steepTransitions[1])
						{
							blendFactorBrush2Steep = 1;
						}
						else
						{
							blendFactorBrush2Steep = 1 - (steepTransitions[1] - steep) / (steepTransitions[1] - steepTransitions[0]);
						}
					}
					else if (height >= heightTransitions[transitionCount - 1])
					{
						brush1Flat = brushesFlat[levels - 1];
						brush2Flat = brushesFlat[levels - 1];
						brush1Steep = brushesSteep[levels - 1];
						brush2Steep = brushesSteep[levels - 1];
						if (steep < steepTransitions[((levels - 1) * 2)])
						{
							blendFactorBrush2Steep = 0;
						} 
						else if (steep > steepTransitions[((levels - 1) * 2) + 1])
						{
							blendFactorBrush2Steep = 1;
						}
						else
						{
							blendFactorBrush2Steep = 1 - (steepTransitions[((levels - 1) * 2) + 1] - steep) / (steepTransitions[((levels - 1) * 2) + 1] - steepTransitions[((levels - 1) * 2)]);
						}
					}
					else
					{
						boolean notFound = true;
						int lastIndex = 0;
						int index = 1;
						while ((index < transitionCount) && notFound)
						{
							if (height <= heightTransitions[index])
							{
								brush1Flat = brushesFlat[(lastIndex + 1) / 2];
								brush2Flat = brushesFlat[(index + 1) / 2];
								brush1Steep = brushesSteep[(lastIndex + 1) / 2];
								brush2Steep = brushesSteep[(index + 1) / 2];
								blendFactorBrush2Flat = ((height - heightTransitions[lastIndex]) / (heightTransitions[index] - heightTransitions[lastIndex]));
								
								if (steep < steepTransitions[(((index-1) / 2) * 2)])
								{
									blendFactorBrush2Steep = 0;
								} 
								else if (steep > steepTransitions[(((index-1) / 2) * 2) + 1])
								{
									blendFactorBrush2Steep = 1;
								}
								else
								{
									blendFactorBrush2Steep = 1 - (steepTransitions[(((index-1) / 2) * 2) + 1] - steep) / (steepTransitions[(((index-1) / 2) * 2) + 1] - steepTransitions[(((index-1) / 2) * 2)]);
								}
								
								notFound = false;
							}
							lastIndex++;
							index++;
						}
					}
					
					//TODO: allow scaling of texture, by supplying ...blabla
					
					//blend color from brushes
					r1 = (brush1Flat.textureR[x % brush1Flat.width][y % brush1Flat.height] & 0xFF);
					g1 = (brush1Flat.textureG[x % brush1Flat.width][y % brush1Flat.height] & 0xFF);
					b1 = (brush1Flat.textureB[x % brush1Flat.width][y % brush1Flat.height] & 0xFF);
					r1s = (brush1Steep.textureR[x % brush1Steep.width][y % brush1Steep.height] & 0xFF);
					g1s = (brush1Steep.textureG[x % brush1Steep.width][y % brush1Steep.height] & 0xFF);
					b1s = (brush1Steep.textureB[x % brush1Steep.width][y % brush1Steep.height] & 0xFF);
					r1 = (int)(((r1 * (1-blendFactorBrush2Steep)) + (r1s * blendFactorBrush2Steep)));
					g1 = (int)(((g1 * (1-blendFactorBrush2Steep)) + (g1s * blendFactorBrush2Steep)));
					b1 = (int)(((b1 * (1-blendFactorBrush2Steep)) + (b1s * blendFactorBrush2Steep)));
					
					r2 = (brush2Flat.textureR[x % brush2Flat.width][y % brush2Flat.height] & 0xFF);
					g2 = (brush2Flat.textureG[x % brush2Flat.width][y % brush2Flat.height] & 0xFF);
					b2 = (brush2Flat.textureB[x % brush2Flat.width][y % brush2Flat.height] & 0xFF);
					r2s = (brush2Steep.textureR[x % brush2Steep.width][y % brush2Steep.height] & 0xFF);
					g2s = (brush2Steep.textureG[x % brush2Steep.width][y % brush2Steep.height] & 0xFF);
					b2s = (brush2Steep.textureB[x % brush2Steep.width][y % brush2Steep.height] & 0xFF);
					r2 = (int)(((r2 * (1-blendFactorBrush2Steep)) + (r2s * blendFactorBrush2Steep)));
					g2 = (int)(((g2 * (1-blendFactorBrush2Steep)) + (g2s * blendFactorBrush2Steep)));
					b2 = (int)(((b2 * (1-blendFactorBrush2Steep)) + (b2s * blendFactorBrush2Steep)));
					
					//set color to texmap
					textureMap[y][(x*3) + 0] = (byte)(((r1 * (1-blendFactorBrush2Flat)) + (r2 * blendFactorBrush2Flat)));
					textureMap[y][(x*3) + 1] = (byte)(((g1 * (1-blendFactorBrush2Flat)) + (g2 * blendFactorBrush2Flat)));
					textureMap[y][(x*3) + 2] = (byte)(((b1 * (1-blendFactorBrush2Flat)) + (b2 * blendFactorBrush2Flat)));
				}
			}
			//Status output
			if ((y % oneTenthsOfHeight) == 0) System.out.print("#");
		}
		System.out.println(" Done generating textureMap ( " + ((System.nanoTime() - start) / 1000000) + " ms )");
	}
	
	/**
	 * 
	 * @param brush if null, whole map will be eroded
	 * @param px x pos of brush
	 * @param py y pos of brush
	 * @param iterations number of loops
	 * @param dropletHeight waterheight of a single droplet
	 * @param evaporateAmount amount of water which evaporates at max each tick
	 */
	public void erodeMapWet(int px, int py, int width, int height)
	{
		ErosionSetup setup = mes.getErosionSetup();
		if (setup == null)
		{
			return;
		}
		if (setup.useAlternativeWetMethod)
		{
			erodeMapDryWet(px, py, width, height, true);
			return;
		}
		
		long start = System.nanoTime();
		
		int iterations = setup.wetIterations;
		float dropletHeight = setup.wetDropletHeight;
		float evaporateAmount = setup.wetEvaporateAmount;
		
		final int oneTenthsOfHeight = Math.max(iterations / 10, 1);
		float[][] waterMap = new float[heightMapWidth][heightMapHeight];
		float dropletSolveFactor = 0.05f;
		float tmpDouble, tmpDouble2;
		boolean foundMoveLocation;
		int xMod, yMod;
		
		int bWidth = width;
		int bHeight = height;
		if (px < 0)
		{
			bWidth = bWidth + px;
			px = 0;
		}
		if (py < 0)
		{
			bHeight = bHeight + py;
			py = 0;
		}
		if (bWidth + px >= heightMapWidth)
			bWidth = heightMapWidth - px;
		if (bHeight + py >= heightMapHeight)
			bHeight = heightMapHeight - py;
			
		for (int i = 0; i < iterations; i++)
		{
			//1. Distribute Water and solve some terrain
			for (int y = py; y < py + bHeight; y++)
			{
				for (int x = px; x < px + bWidth; x++)
				{
					if ((x >= 0) && (x < heightMapWidth) && (y >= 0) && (y < heightMapHeight))
					{
						if (heightMap[x][y] > (dropletHeight * dropletSolveFactor))
						{
							waterMap[x][y] = waterMap[x][y] + dropletHeight;
							heightMap[x][y] = heightMap[x][y] - (dropletHeight * dropletSolveFactor);
						}
					}
				}
			}
			
			//2. Move Water
			for (int y = py; y < py + bHeight; y++)
			{
				for (int x = px; x < px + bWidth; x++)
				{
					if ((x >= 0) && (x < heightMapWidth) && (y >= 0) && (y < heightMapHeight))
					{
						xMod = 0;
						yMod = 0;
						foundMoveLocation = false;
						tmpDouble =  heightMap[x][y] + waterMap[x][y]; //Waterheight on center
						
						if ((x-1 >= px) && (y-1 >= py))
						{
							tmpDouble2 = heightMap[x - 1][y - 1] + waterMap[x - 1][y - 1];
							if (tmpDouble > tmpDouble2) { xMod = -1; yMod = -1; tmpDouble = tmpDouble2; foundMoveLocation = true; }
						}
						
						if ((x+1 < px+bWidth) && (y-1 >= py))
						{
							tmpDouble2 = heightMap[x + 1][y - 1] + waterMap[x + 1][y - 1];
							if (tmpDouble > tmpDouble2) { xMod = +1; yMod = -1; tmpDouble = tmpDouble2; foundMoveLocation = true; }
						}
						
						if ((x+1 < px+bWidth) && (y+1 < py+bHeight))
						{
							tmpDouble2 = heightMap[x + 1][y + 1] + waterMap[x + 1][y + 1];
							if (tmpDouble > tmpDouble2) { xMod = 1; yMod = 1; tmpDouble = tmpDouble2; foundMoveLocation = true; }
						}
						
						if ((x-1 >= px) && (y+1 < py+bHeight))
						{
							tmpDouble2 = heightMap[x - 1][y + 1] + waterMap[x - 1][y + 1];
							if (tmpDouble > tmpDouble2) { xMod = -1; yMod = 1; tmpDouble = tmpDouble2; foundMoveLocation = true; }
						}
						
						//Even out Waterlevels between two locations
						if (foundMoveLocation)
						{
							if (heightMap[x + xMod][y + yMod] < heightMap[x][y])
							{
								//Height difference
								tmpDouble = heightMap[x][y] - heightMap[x + xMod][y + yMod];
								//Available Water
								tmpDouble2 = waterMap[x + xMod][y + yMod] + waterMap[x][y];
								if (tmpDouble > tmpDouble2)
								{
									//All water fits in new location
									waterMap[x + xMod][y + yMod] = tmpDouble2;
									waterMap[x][y] = 0;
								}
								else
								{
									//Distribute evenly
									waterMap[x + xMod][y + yMod] = tmpDouble;
									tmpDouble2 = tmpDouble2 - tmpDouble;
									waterMap[x + xMod][y + yMod] = waterMap[x + xMod][y + yMod] + (tmpDouble2 / 2);
									waterMap[x][y] = (tmpDouble2 / 2);
								}
							}
							else
							{
								//Height difference
								tmpDouble = heightMap[x + xMod][y + yMod] - heightMap[x][y];
								//Available Water
								tmpDouble2 = waterMap[x + xMod][y + yMod] + waterMap[x][y];
								if (tmpDouble > tmpDouble2)
								{
									//All water fits in old location (should never happen)
									waterMap[x + xMod][y + yMod] = 0;
									waterMap[x][y] = tmpDouble2;
								}
								else
								{
									//Distribute evenly
									waterMap[x][y] = tmpDouble;
									tmpDouble2 = tmpDouble2 - tmpDouble;
									waterMap[x][y] = waterMap[x][y] + (tmpDouble2 / 2);
									waterMap[x + xMod][y + yMod] = (tmpDouble2 / 2);
								}
							}
						}
					}
				}
			}
			
			//3. Evaporate some Water
			for (int y = py - 1; y < py + bHeight + 1; y++)
			{
				for (int x = px - 1; x < px + bWidth + 1; x++)
				{
					if ((x >= 0) && (x < heightMapWidth) && (y >= 0) && (y < heightMapHeight))
					{
						tmpDouble = Math.min(waterMap[x][y], evaporateAmount);
						waterMap[x][y] = waterMap[x][y] - tmpDouble;
						heightMap[x][y] = heightMap[x][y] + (tmpDouble * dropletSolveFactor);
					}
				}
			}
			
			//Status output
			if ((i % oneTenthsOfHeight) == 0) System.out.print("#");
		}
		//4. Cleanup: Evaporate all water
		for (int y = py - 1; y < py + bHeight + 1; y++)
		{
			for (int x = px - 1; x < px + bWidth + 1; x++)
			{
				if ((x >= 0) && (x < heightMapWidth) && (y >= 0) && (y < heightMapHeight))
				{
					heightMap[x][y] = heightMap[x][y] + (waterMap[x][y] * dropletSolveFactor);
				}
			}
		}
		System.out.println(" Done eroding heightmap ( " + ((System.nanoTime() - start) / 1000000) + " ms )");
	}
	
	/**
	 * Potentially faster erosion implementation
	 * @param brush if null, whole map will be eroded
	 * @param px x pos of brush
	 * @param py y pos of brush
	 * @param iterations number of loops
	 * @param breakHeight height difference which leads to erosion
	 * @param hydroErosion set to true for hydraulic erosion, set to false for thermic erosion
	 */
	public void erodeMapDryWet(int px, int py, int width, int height, boolean hydroErosion)
	{
		ErosionSetup setup = mes.getErosionSetup();
		if (setup == null)
		{
			return;
		}
		
		long start = System.nanoTime();
		
		int iterations = setup.dryIterations;
	    float breakHeight = setup.dryBreakHeight;
	    if (hydroErosion)
	    {
	    	iterations = setup.wet2Iterations;
		    breakHeight = setup.wet2BreakHeight;
	    }
	    
		final int oneTenthsOfHeight = Math.max(iterations / 10, 1);
		
		float tmpDouble;
		int xMod, yMod;
		
		int bWidth = width;
		int bHeight = height;
		if (px < 0)
		{
			bWidth = bWidth + px;
			px = 0;
		}
		if (py < 0)
		{
			bHeight = bHeight + py;
			py = 0;
		}
		if (bWidth + px >= heightMapWidth)
			bWidth = heightMapWidth - px;
		if (bHeight + py >= heightMapHeight)
			bHeight = heightMapHeight - py;
		for (int i = 0; i < iterations; i++)
		{
			//1. Move soil
			for (int y = py; y < py + bHeight; y++)
			{
				for (int x = px; x < px + bWidth; x++)
				{
					if ((x >= 0) && (x < heightMapWidth) && (y >= 0) && (y < heightMapHeight))
					{
						xMod = 0;
						yMod = 0;
						tmpDouble = 0; //max height distance so far
						
						if ((x-1 >= px) && (y-1 >= py))
						{
							if ((heightMap[x][y] - heightMap[x - 1][y - 1]) > tmpDouble)
							{
								tmpDouble = heightMap[x][y] - heightMap[x - 1][y - 1];
								xMod = -1; 
								yMod = -1;
							}
						}
						
						if ((x+1 < px+bWidth) && (y-1 >= py))
						{
							if ((heightMap[x][y] - heightMap[x + 1][y - 1]) > tmpDouble)
							{
								tmpDouble = heightMap[x][y] - heightMap[x + 1][y - 1];
								xMod = +1; 
								yMod = -1;
							}
						}
						
						if ((x+1 < px+bWidth) && (y+1 < py+bHeight))
						{
							if ((heightMap[x][y] - heightMap[x + 1][y + 1]) > tmpDouble)
							{
								tmpDouble = heightMap[x][y] - heightMap[x + 1][y + 1];
								xMod = +1; 
								yMod = +1;
							}
						}
						
						if ((x-1 >= px) && (y+1 < py+bHeight))
						{
							if ((heightMap[x][y] - heightMap[x - 1][y + 1]) > tmpDouble)
							{
								tmpDouble = heightMap[x][y] - heightMap[x - 1][y + 1];
								xMod = -1; 
								yMod = +1;
							}
						}
						
						//Even out heightlevels between two locations
						if (hydroErosion)
						{
							if ((tmpDouble > 0) && (tmpDouble < breakHeight))
							{
								tmpDouble = heightMap[x + xMod][y + yMod] + heightMap[x][y];
								heightMap[x + xMod][y + yMod] = tmpDouble / 2;
								heightMap[x][y] = tmpDouble / 2;
							}
						}
						else
						{
							if ((tmpDouble > 0) && (tmpDouble > breakHeight))
							{
								tmpDouble = heightMap[x + xMod][y + yMod] + heightMap[x][y];
								heightMap[x + xMod][y + yMod] = tmpDouble / 2;
								heightMap[x][y] = tmpDouble / 2;
							}
						}
					}
				}
			}
			//Status output
			if ((i % oneTenthsOfHeight) == 0) System.out.print("#");
		}
		System.out.println(" Done eroding heightmap ( " + ((System.nanoTime() - start) / 1000000) + " ms )");
	}
		
	public void ttdize(int stepCount)
	{
		long start = System.nanoTime();
		int x, y;
		float steps = stepCount;
		int[][] steppedMap = new int[heightMapWidth][heightMapHeight];
		//Round heights to steps
		for (y = 0; y < heightMapHeight; y++)
		{
			for (x = 0; x < heightMapWidth; x++)
			{
				steppedMap[x][y] = FastMath.round(heightMap[x][y] * steps);
			}
		}
		//enforce max steepness
		for (y = 1; y < heightMapHeight; y++)
		{
			for (x = 1; x < heightMapWidth; x++)
			{
				//Clamp to upper
				if (steppedMap[x][y] > steppedMap[x][y-1])
					steppedMap[x][y] = steppedMap[x][y-1] + 1;
				else if (steppedMap[x][y] < steppedMap[x][y-1])
					steppedMap[x][y] = steppedMap[x][y-1] - 1;
				
				//Clamp to left
				if (steppedMap[x][y] > steppedMap[x-1][y])
					steppedMap[x][y] = steppedMap[x-1][y] + 1;
				else if (steppedMap[x][y] < steppedMap[x-1][y])
					steppedMap[x][y] = steppedMap[x-1][y] - 1;
			}
		}
		//copy back
		for (y = 0; y < heightMapHeight; y++)
		{
			for (x = 0; x < heightMapWidth; x++)
			{
				heightMap[x][y] = steppedMap[x][y] / steps;
			}
		}
		System.out.println("Done ttdizing heightMap ( " + ((System.nanoTime() - start) / 1000000) + " ms )");
	}
	
	public void randomizeHeight(int px, int py, int width, int height, float amount)
	{
		Random r = new Random();
		float amountHalf = amount / 2;
		for (int y = py; y < py + height; y++)
		{
			for (int x = px; x < px + width; x++)
			{
				if ((x > 0) && (x < heightMapWidth-1) && (y > 0) && (y < heightMapHeight-1))
				{
					heightMap[x][y] = heightMap[x][y] + (r.nextFloat() * amount) - amountHalf;
				}
			}
		}
	}
	
	public void whiteOutTextureMap()
	{
		long start = System.nanoTime();
		for (int y = 0; y < textureMapHeight; y++)
		{
			Arrays.fill(textureMap[y], (byte)255);
		}
		System.out.println("Done blanking texture ( " + ((System.nanoTime() - start) / 1000000) + " ms )");
	}
	
	public void blankFeatureMap()
	{
		long start = System.nanoTime();
		for (int i = 0; i < featureBlockCount; i++)
		{
			featureList[i].clear();
		}
		System.out.println("Done blanking featureMap ( " + ((System.nanoTime() - start) / 1000000) + " ms )");
	}
	
	/**
	 * Returns block coordinates at given x/z or null if outside bounds
	 * @param x
	 * @param z
	 * @return
	 */
	private Point getFeatureBlockByCoords(float x, float z)
	{
		int bx = (int)((x / tileSize) / featureBlockSizeInTiles);
		int by = (int)((z / tileSize) / featureBlockSizeInTiles);
		if ((bx >= 0) && (bx < featureMapWidthInBlocks) && (by >= 0) && (by < featureMapHeightInBlocks))
			return new Point(bx, by);
		else
			return null;
	}
	
	public void addFeature(float x, float z, float rotY, int featureID)
	{
		Point block = getFeatureBlockByCoords(x, z);
		if ((block.x >= 0) && (block.x < featureMapWidthInBlocks) && (block.y >= 0) && (block.y < featureMapHeightInBlocks))
			featureList[block.x + (block.y * featureMapWidthInBlocks)].add(new FeatureMapContainer(x, 0, z, rotY, featureID));
	}
	
	/**
	 * returns FeatureMapContainer at given location or null if outside map
	 * @param x
	 * @param z
	 * @param maxDist
	 * @return
	 */
	private FeatureMapContainer findClosestFeature(float x, float z, float maxDist)
	{
		float left = x - maxDist;
		float top = z - maxDist;
		float right = x + maxDist;
		float bottom = z + maxDist;
		Point leftUpperBlock = getFeatureBlockByCoords(left, top);
		Point rightLowerBlock = getFeatureBlockByCoords(right, bottom);
		
		// Check for valid block
		if (leftUpperBlock == null) return null;
		if (rightLowerBlock == null) return null;
		
		int blockX, blockY;
		Iterator<FeatureMapContainer> it;
		FeatureMapContainer f;
		float dist;
		float curMinDist = maxDist * 2;
		FeatureMapContainer result = null;
		for (blockY = leftUpperBlock.y; blockY <= rightLowerBlock.y; blockY++)
		{
			for (blockX = leftUpperBlock.x; blockX <= rightLowerBlock.x; blockX++)
			{
				it = featureList[blockX + (blockY * featureMapWidthInBlocks)].iterator();
				while (it.hasNext())
				{
					f = it.next();
					dist = Math.abs(f.x - x) + Math.abs(f.z - z);
					if ((dist < curMinDist) && (Math.abs(f.x - x) <= maxDist) && (Math.abs(f.z - z) <= maxDist))
					{
						curMinDist = dist;
						result = f;
					}
				}
			}
		}
		return result;
	}
	
	public void removeAllFeaturesInRange(float x, float z, float maxDist)
	{
		FeatureMapContainer f = findClosestFeature(x, z, maxDist);
		while (f != null)
		{
			Point block = getFeatureBlockByCoords(f.x, f.z);
			featureList[block.x + (block.y * featureMapWidthInBlocks)].remove(f);
			
			f = findClosestFeature(x, z, maxDist);
		}
	}
	
	public void removeClosestFeature(float x, float z, float maxDist)
	{
		FeatureMapContainer f = findClosestFeature(x, z, maxDist);
		if (f != null)
		{
			Point block = getFeatureBlockByCoords(f.x, f.z);
			featureList[block.x + (block.y * featureMapWidthInBlocks)].remove(f);
		}
	}
	
	public void rotateClosestFeature(float x, float z, float maxDist, float amount)
	{
		FeatureMapContainer f = findClosestFeature(x, z, maxDist);
		if (f != null)
		{
			f.rotY += amount;
			if (f.rotY > 360)
				f.rotY -= 360;
			if (f.rotY < 0)
				f.rotY += 360;
		}
	}
	
	public void genStartupHeightmap()
	{
		TerraGenSetup setup = mes.getTerraGenSetup();
		if (setup == null)
		{
			return;
		}
		genDiamondSquareRandom(heightMap, heightMapWidth, heightMapHeight, setup.maxDisplacement, setup.displacementRegression, setup.skipSteps, setup.randomSeed);
	}
	
	public void genRandom()
	{
		Random r = new Random();
		int x, y;
		for (y = 0; y < heightMapHeight; y++)
		{
			for (x = 0; x < heightMapWidth; x++)
			{
				heightMap[x][y] = r.nextFloat();
				if ((x > 20) && (x < 40) && (y > 20) && (y < 40))
				{
					heightMap[x][y] = 1;
				}
			}
		}
	}
	
	public void genDiamondSquareRandom(float[][] map, int width, int height, float maxDisplacement, float displacementRegression, int skipSteps, int randomSeed)
	{
		/*
		 * 1. Calculate next larger power of 2 square, which can contain given map.
		 * 2. Allocate new array with this size, copy existing into it, init everything outside via mirror copy
		 * 3. Generate via diamond-square
		 * 4. Copy back
		 */
		
		//Get next larger power of 2 squaresize
		int squareSize = Math.max(width - 1, height - 1);
		squareSize = FastMath.pow(2, (int)Math.ceil(Math.log(squareSize) / Math.log(2))); 
		final int size = squareSize;
		final int arraySize = squareSize + 1;
		
		//Allocate temporary array
		float[][] tempMap = new float[arraySize][arraySize];
		
		//Copy existing map to new map
		int x, y;
		for (x = 0; x < width; x++)
			System.arraycopy(map[x], 0, tempMap[x], 0, height);
		
		//Mirror-Copy outside area. (This is better than initializing to some fixed value)
		if (width < arraySize)
		{
			for (x = width; x < arraySize; x++)
			{
				for (y = 0; y < arraySize; y++)
				{
					if (((x / (width - 1)) % 2) == 0)
						tempMap[x][y] = tempMap[x % (width - 1)][y];
					else
						tempMap[x][y] = tempMap[width - (x % (width - 1))][y];
				}
			}
		}
		if (height < arraySize)
		{
			for (y = height; y < arraySize; y++)
			{
				for (x = 0; x < arraySize; x++)
				{
					if (((y / (height - 1)) % 2) == 0)
						tempMap[x][y] = tempMap[x][y % (height - 1)];
					else
						tempMap[x][y] = tempMap[x][(height - (y % (height - 1)))];
				}
			}
		}
		
		//Do Diamond Square
		Random r = new Random();
		if (randomSeed >= 0) r = new Random(randomSeed);
		int[] xPos = new int[4];
		int[] yPos = new int[4];
		int halfSize;
		float maxDisplacementHalf = maxDisplacement / 2;
		while (squareSize > 1)
		{
			halfSize = squareSize / 2;
			if (skipSteps <= 0)
			{
				for (y = 0; y < size; y += squareSize)
				{
					yPos[0] = (y - halfSize); if (yPos[0] < 0) yPos[0] = size + yPos[0];
					yPos[1] = y + halfSize;
					yPos[2] = y + squareSize;
					yPos[3] = y + squareSize + halfSize; if (yPos[3] > size) yPos[3] = yPos[3] % size;
					for (x = 0; x < size; x += squareSize)
					{
						xPos[0] = (x - halfSize); if (xPos[0] < 0) xPos[0] = size + xPos[0];
						xPos[1] = x + halfSize;
						xPos[2] = x + squareSize;
						xPos[3] = x + squareSize + halfSize; if (xPos[3] > size) xPos[3] = xPos[3] % size;

						//Square Step
						//Center
						tempMap[xPos[1]][yPos[1]] = Math.min(1, Math.max(0, ((tempMap[x][y] + tempMap[x][yPos[2]] + tempMap[xPos[2]][y] + tempMap[xPos[2]][yPos[2]]) / 4) + ((r.nextFloat() * maxDisplacement) - maxDisplacementHalf)));
						
						//Diamond(s) Step
						//Left
						//tempMap[x][yPos[1]] = Math.min(1, Math.max(0, ((tempMap[x][y] + tempMap[x][yPos[2]]) / 2)));
						tempMap[x][yPos[1]] = Math.min(1, Math.max(0, ((tempMap[x][y] + tempMap[x][yPos[2]]) / 2) + ((r.nextFloat() * maxDisplacement) - maxDisplacementHalf)));
						//tempMap[x][yPos[1]] = Math.min(1, Math.max(0, ((tempMap[x][y] + tempMap[x][yPos[2]] + tempMap[xPos[1]][yPos[1]]) / 3) + ((r.nextFloat() * maxDisplacement) - maxDisplacementHalf)));
						//tempMap[x][yPos[1]] = Math.min(1, Math.max(0, ((tempMap[x][y] + tempMap[x][yPos[2]] + tempMap[xPos[1]][yPos[1]] + tempMap[xPos[0]][yPos[1]]) / 4) + ((r.nextFloat() * maxDisplacement) - maxDisplacementHalf)));
						
						//Upper
						//tempMap[xPos[1]][y] = Math.min(1, Math.max(0, ((tempMap[x][y] + tempMap[xPos[2]][y]) / 2)));
						tempMap[xPos[1]][y] = Math.min(1, Math.max(0, ((tempMap[x][y] + tempMap[xPos[2]][y]) / 2) + ((r.nextFloat() * maxDisplacement) - maxDisplacementHalf)));
						//tempMap[xPos[1]][y] = Math.min(1, Math.max(0, ((tempMap[x][y] + tempMap[xPos[1]][yPos[1]] + tempMap[xPos[2]][y]) / 3) + ((r.nextFloat() * maxDisplacement) - maxDisplacementHalf)));
						//tempMap[xPos[1]][y] = Math.min(1, Math.max(0, ((tempMap[x][y] + tempMap[xPos[1]][yPos[0]] + tempMap[xPos[1]][yPos[1]] + tempMap[xPos[2]][y]) / 4) + ((r.nextFloat() * maxDisplacement) - maxDisplacementHalf)));
						
						//Lower
						//tempMap[xPos[1]][yPos[2]] = Math.min(1, Math.max(0, ((tempMap[x][yPos[2]] + tempMap[xPos[2]][yPos[2]]) / 2)));
						tempMap[xPos[1]][yPos[2]] = Math.min(1, Math.max(0, ((tempMap[x][yPos[2]] + tempMap[xPos[2]][yPos[2]]) / 2) + ((r.nextFloat() * maxDisplacement) - maxDisplacementHalf)));
						//tempMap[xPos[1]][yPos[2]] = Math.min(1, Math.max(0, ((tempMap[x][yPos[2]] + tempMap[xPos[1]][yPos[1]] + tempMap[xPos[2]][yPos[2]]) / 3) + ((r.nextFloat() * maxDisplacement) - maxDisplacementHalf)));
						//tempMap[xPos[1]][yPos[2]] = Math.min(1, Math.max(0, ((tempMap[x][yPos[2]] + tempMap[xPos[1]][yPos[1]] + tempMap[xPos[2]][yPos[2]] + tempMap[xPos[1]][yPos[3]]) / 4) + ((r.nextFloat() * maxDisplacement) - maxDisplacementHalf)));
						
						//Right
						//tempMap[xPos[2]][yPos[1]] = Math.min(1, Math.max(0, ((tempMap[xPos[2]][y] + tempMap[xPos[2]][yPos[2]]) / 2)));
						tempMap[xPos[2]][yPos[1]] = Math.min(1, Math.max(0, ((tempMap[xPos[2]][y] + tempMap[xPos[2]][yPos[2]]) / 2) + ((r.nextFloat() * maxDisplacement) - maxDisplacementHalf)));
						//tempMap[xPos[2]][yPos[1]] = Math.min(1, Math.max(0, ((tempMap[xPos[2]][y] + tempMap[xPos[2]][yPos[2]] + tempMap[xPos[1]][yPos[1]]) / 3) + ((r.nextFloat() * maxDisplacement) - maxDisplacementHalf)));
						//tempMap[xPos[2]][yPos[1]] = Math.min(1, Math.max(0, ((tempMap[xPos[2]][y] + tempMap[xPos[2]][yPos[2]] + tempMap[xPos[1]][yPos[1]] + tempMap[xPos[3]][yPos[1]]) / 4) + ((r.nextFloat() * maxDisplacement) - maxDisplacementHalf)));
					}
				}
			}
			else
				skipSteps--;
			
			squareSize = halfSize;
			maxDisplacement = maxDisplacement * displacementRegression;
			maxDisplacementHalf = maxDisplacement / 2;
		}
		
		//Copy back to given array
		for (x = 0; x < width; x++)
			System.arraycopy(tempMap[x], 0, map[x], 0, height);
	}
	
	@SuppressWarnings("unchecked")
	public void backupToMemory()
	{
		try
		{
			long start = System.nanoTime();
			
			//Init storage
			quicksave.prepareSave();
			
			//Compression
			OutputStream compressionStream = null;
			if (as.quicksave_compress)
			{
				quicksave.isCompressed = true;
				compressionStream = new BufferedOutputStream(new DeflaterOutputStream(quicksave.backupStorage, new Deflater(Deflater.BEST_SPEED, true), QuickSave.BACKUP_BLOCK_SIZE), QuickSave.BACKUP_BLOCK_SIZE);
			}
			else
			{
				compressionStream = new DataOutputStream(quicksave.backupStorage);
			}
				
			//DataOutputStream 
			DataOutputStream dataStream = new DataOutputStream(compressionStream);
			
			//Store heightmap
			if (as.quicksave_heightmap)
			{
				for (int x = 0; x < heightMapWidth; x++)
					dataStream.write(heightMap[x], 0, heightMapHeight);
				quicksave.hasHeightmap = true;
			}
			//Store Texturemap
			if (as.quicksave_texturemap)
			{
				for (int y = 0; y < textureMapHeight; y++)
					dataStream.write(textureMap[y], 0, textureMapWidth * 3);
				quicksave.hasTexturemap = true;
			}
			//Store Metalmap
			if (as.quicksave_metalmap)
			{
				for (int x = 0; x < metalMapWidth; x++)
					dataStream.write(metalMap[x], 0, metalMapHeight);
				quicksave.hasMetalmap = true;
			}
			//Store Typemap
			if (as.quicksave_typemap)
			{
				for (int x = 0; x < typeMapWidth; x++)
					dataStream.write(typeMap[x], 0, typeMapHeight);
				quicksave.hasTypemap = true;
			}
			//Store Vegetationmap
			if (as.quicksave_vegetationmap)
			{
				for (int x = 0; x < vegetationMapWidth; x++)
					dataStream.write(vegetationMap[x], 0, vegetationMapHeight);
				quicksave.hasVegetationmap = true;
			}
			dataStream.flush();
			compressionStream.close();
			
			//Store features
			if (as.quicksave_featuremap)
			{
				if (quicksave.featureListBackup != null)
					for (int i = 0; i < featureBlockCount; i++) quicksave.featureListBackup[i] = null;
				quicksave.featureListBackup = (ArrayList<FeatureMapContainer>[])new ArrayList[featureBlockCount];
				for (int i = 0; i < featureBlockCount; i++)
					quicksave.featureListBackup[i] = new ArrayList<FeatureMapContainer>();
				for (int i = 0; i < featureBlockCount; i++)
				{
					Iterator<FeatureMapContainer> it = featureList[i].iterator();
					while (it.hasNext())
					{
						quicksave.featureListBackup[i].add((FeatureMapContainer)it.next().clone());
					}
				}
				quicksave.hasFeaturemap = true;
			}
			quicksave.backupStored = true;
			System.out.println("Done Quicksaving. ( " + ((System.nanoTime() - start) / 1000000) + " ms ): " + "Backup Size: " + (quicksave.backupStorage.size() / 1024 / 1024) + " MB used");
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (CloneNotSupportedException e)
		{
			e.printStackTrace();
		}
	}
	
	public boolean[] restoreFromMemory()
	{
		boolean[] result = new boolean[3];
		result[0] = false;
		result[1] = false;
		result[2] = false;
		
		if (quicksave.backupStored)
		{
			try
			{
				long start = System.nanoTime();
				
				ByteInputStream backupInput = quicksave.backupStorage.getByteInputStream();
				InputStream deCompressionStream = null;
				if (quicksave.isCompressed)
					deCompressionStream = new BufferedInputStream(new InflaterInputStream(backupInput, new Inflater(true), QuickSave.BACKUP_BLOCK_SIZE), QuickSave.BACKUP_BLOCK_SIZE);
				else
					deCompressionStream = new DataInputStream(backupInput);
				
				//DataInputStream 
				DataInputStream dataStream = new DataInputStream(deCompressionStream);
				
				//Restore heightmap
				if (quicksave.hasHeightmap)
				{
					for (int x = 0; x < heightMapWidth; x++)
						dataStream.read(heightMap[x], 0, heightMapHeight);
					result[0] = true;
				}
				//Restore Texturemap
				if (quicksave.hasTexturemap)
				{
					for (int y = 0; y < textureMapHeight; y++)
						dataStream.read(textureMap[y], 0, textureMapWidth * 3);
					result[1] = true;
				}
				//Restore Metalmap
				if (quicksave.hasMetalmap)
				{
					for (int x = 0; x < metalMapWidth; x++)
						dataStream.read(metalMap[x], 0, metalMapHeight);
					result[1] = true;
				}
				//Restore Typemap
				if (quicksave.hasTypemap)
				{
					for (int x = 0; x < typeMapWidth; x++)
						dataStream.read(typeMap[x], 0, typeMapHeight);
					result[1] = true;
				}
				//Restore Vegetationmap
				if (quicksave.hasVegetationmap)
				{
					for (int x = 0; x < vegetationMapWidth; x++)
						dataStream.read(vegetationMap[x], 0, vegetationMapHeight);
					result[1] = true;
				}
				//Restore features
				if (quicksave.hasFeaturemap)
				{
					for (int i = 0; i < featureBlockCount; i++) featureList[i].clear();
					for (int i = 0; i < featureBlockCount; i++)
					{
						Iterator<FeatureMapContainer> it = quicksave.featureListBackup[i].iterator();
						while (it.hasNext())
						{
							featureList[i].add((FeatureMapContainer)it.next().clone());
						}
					}
					result[2] = true;
				}
				System.out.println("Done Quickloading. ( " + ((System.nanoTime() - start) / 1000000) + " ms )");
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			catch (CloneNotSupportedException e)
			{
				e.printStackTrace();
			}
		}
		
		return result;
	}
}
