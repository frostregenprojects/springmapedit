/*
 * SpringMapEdit -- A 3D map editor for the Spring engine
 *
 * Copyright (C) 2008-2009  Heiko Schmitt <heikos23@web.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
/**
 * MapEditSettings.java 
 * Created on 10.10.2008
 * by Heiko Schmitt
 */
package backend;

import java.io.File;
import java.io.IOException;

import backend.math.Vector2Int;

import frontend.render.AppSettings;
import frontend.render.brushes.Brush;
import frontend.render.brushes.BrushDataManager;
import frontend.render.brushes.BrushPattern;
import frontend.render.brushes.BrushPatternFactory;
import frontend.render.brushes.BrushTexture;
import frontend.render.brushes.BrushTextureFactory;
import frontend.render.brushes.FeatureBrush;
import frontend.render.brushes.HeightBrush;
import frontend.render.brushes.MetalBrush;
import frontend.render.brushes.PrefabBrush;
import frontend.render.brushes.PrefabManager;
import frontend.render.brushes.TextureBrush;
import frontend.render.brushes.TypeBrush;
import frontend.render.brushes.VegetationBrush;

/**
 * @author Heiko Schmitt
 *
 */
public class MapEditSettings
{
	//Brush Managers
	public BrushDataManager<BrushPattern> brushPatternManager;
	public BrushDataManager<BrushTexture> brushTextureManager;
	public PrefabManager prefabManager;
	
	//TODO: mabe make this float?
	public Vector2Int brushPos;
	public Vector2Int brushAlign;
	public float brushHeightAlign;
	public boolean useAlign;
	
	//Brush currently in use
	public Brush activeBrush;
	private BrushMode brushMode;
	
	//Brushes
	private HeightBrush heightBrush;
	private TextureBrush textureBrush;
	private MetalBrush metalBrush; 
	private TypeBrush typeBrush;
	private VegetationBrush vegetationBrush;
	private FeatureBrush featureBrush;
	private PrefabBrush prefabBrush;
	
	//Texture Generator
	private TextureGeneratorSetup textureGeneratorSetup;
	
	//Erosion
	private ErosionSetup erosionSetup;
	
	//Terra Generator
	private TerraGenSetup terraGenSetup;
	
	//UI Info (default brush pattern/texture icon size)
	public final int defaultSize = 100;
	
	//Settings
	private AppSettings as;
	
	public enum BrushMode
	{
		Height,
		Texture,
		Metal,
		Type,
		Vegetation,
		Feature,
		Prefab
	}
	
	public MapEditSettings(AppSettings as)
	{
		this.as = as;
		init();
	}

	private void init()
	{
		brushPatternManager = new BrushDataManager<BrushPattern>(BrushDataManager.brushPatternPath, new BrushPatternFactory(), "Pattern", as);
		brushTextureManager = new BrushDataManager<BrushTexture>(BrushDataManager.brushTexturePath, new BrushTextureFactory(), "Texture", as);
		prefabManager = new PrefabManager(PrefabManager.defaultPrefabPath, as);
		
		//Initial Position
		brushPos = new Vector2Int(5, 5);
		useAlign = false;
		brushAlign = new Vector2Int(1, 1);
		brushHeightAlign = 0.001f;
		
		//initialize all brushes
		heightBrush = new HeightBrush(brushPatternManager, 0, 20, 20);
		textureBrush = new TextureBrush(brushPatternManager, brushTextureManager, 0, 0, 20, 20, -1, -1);
		metalBrush = new MetalBrush(brushPatternManager, 0, 20, 20);
		typeBrush = new TypeBrush(1, 20, 20);
		vegetationBrush = new VegetationBrush(1, 20, 20);
		featureBrush = new FeatureBrush(0, 10, 10);
		prefabBrush = new PrefabBrush(prefabManager, 0, -1, -1);
		
		//Setup main mode
		setBrushMode(BrushMode.Height);
		
		//Setup Texture Generator
		setTextureGeneratorSetupByFile(new File(TextureGeneratorSetup.textureScriptPath + "default.tdf"));
		
		//Setup Erosion
		setErosionSetupByFile(new File(ErosionSetup.erosionScriptPath + "default.tdf"));
		
		//Setup TerraGen
		terraGenSetup = new TerraGenSetup();
	}
	
	public BrushMode getBrushMode()
	{
		return brushMode;
	}
	
	public void setBrushMode(BrushMode brushMode)
	{
		this.brushMode = brushMode;
		useAlign = false;
		switch (brushMode)
		{
			case Height: activeBrush = heightBrush; break;
			case Texture: activeBrush = textureBrush; break;
			case Metal: activeBrush = metalBrush; break;
			case Type: activeBrush = typeBrush; break;
			case Vegetation: activeBrush = vegetationBrush; break;
			case Feature: activeBrush = featureBrush; break;
			case Prefab: activeBrush = prefabBrush; useAlign = true; break;
		}
	}
	
	public void setTextureGeneratorSetupByFile(File tgsFile)
	{
		try
		{
			textureGeneratorSetup = new TextureGeneratorSetup(tgsFile);
			textureGeneratorSetup.loadBrushTextures();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			textureGeneratorSetup = null;
		}
	}
	
	public void setErosionSetupByFile(File esFile)
	{
		try
		{
			erosionSetup = new ErosionSetup(esFile);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			erosionSetup = null;
		}
	}
	
	//////////////////
	//Brush Getters
	//////////////////
	public HeightBrush getHeightBrush()
	{
		return heightBrush;
	}
	
	public TextureBrush getTextureBrush()
	{
		return textureBrush;
	}
	
	public MetalBrush getMetalBrush()
	{
		return metalBrush;
	}
	
	public TypeBrush getTypeBrush()
	{
		return typeBrush;
	}
	
	public VegetationBrush getVegetationBrush()
	{
		return vegetationBrush;
	}
	
	public FeatureBrush getFeatureBrush()
	{
		return featureBrush;
	}
	
	public PrefabBrush getPrefabBrush()
	{
		return prefabBrush;
	}
	
	public TextureGeneratorSetup getTextureGeneratorSetup()
	{
		return textureGeneratorSetup;
	}
	
	public ErosionSetup getErosionSetup()
	{
		return erosionSetup;
	}
	
	public TerraGenSetup getTerraGenSetup()
	{
		return terraGenSetup;
	}
	
	public void setBrushPos(int x, int y)
	{
		if (useAlign)
			brushPos.set(FastMath.round(x / (double)brushAlign.x()) * brushAlign.x(), FastMath.round(y / (double)brushAlign.y()) * brushAlign.y());
		else
			brushPos.set(x, y);
	}
}
